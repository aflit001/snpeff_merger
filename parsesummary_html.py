#!/usr/bin/python

import argparse
import os, sys
import re

import array
import numpy
import types
import copy

import simplejson
import jsonpickle
import unicodedata

from bs4 import BeautifulSoup as bs

jsonpickle.set_preferred_backend('simplejson')
jsonpickle.set_encoder_options(  'simplejson', sort_keys=True, indent=' ', ensure_ascii=True, encoding='utf-8' )


def main():
    dic = {}

    try:
        outfile = sys.argv[1]
    except:
        print "no outfile given"
        sys.exit(1)

    if os.path.exists( outfile ):
        print "oufile exists"
        sys.exit(1)

    try:
        infiles = sys.argv[2:]
    except:
        print "no infiles"
        sys.exit(1)

    for filename in infiles:
        if not os.path.exists( filename ):
            print "infile %s does not exists" % filename
            sys.exit(1)
        if os.path.getsize( filename ) == 0:
            print "infile %s has size 0" % filename
            sys.exit(2)
        if not filename.endswith( '.html' ):
            print "infile %s is not html" % filename
            sys.exit(1)

    for filename in sorted(infiles):
        print "F %s" % filename

        dic[ filename ] = {}

        data = open(filename, 'r').read()
        data = fixshittihtml(data)

        soup = bs(data, 'xml')

        for table in soup.find_all('table', recursive=True):
            #continue
            name, title = getNameAndTitle(table)
            if name  is None: continue
            if title is None: continue

            print "parsing %s :: Table :: A name : %s" % ( filename, name  )
            print "parsing %s :: Table :: B title: %s" % ( filename, title )
            print

            tdata   = prettytable(filename, name, title)
            headers = []

            for thead in table.find_all(['thead', 'tfoot']):
                for tr in thead.find_all('tr'):
                    theaderline = []
                    for th in thead.find_all('th'):
                        ttext = th.find_all(text=True)
                        ttext = [] if ttext is None else ttext
                        text  = ''.join(ttext)
                        text  = parsetext( text )
                        theaderline.append(text)
                    headers.append( theaderline )

            hasTable = False
            rows     = table.find_all(['tr'])
            lines    = []
            for tr in rows:
                if tr.parent.name in ['thead', 'tfoot']:
                    continue

                cols  = tr.find_all(['td', 'th'])
                tline = []
                for td in cols:
                    if td.find('table') is not None:
                        #print tr
                        #print td.find('table')
                        hasTable = True

                    if hasTable: break
                    ttext = td.find_all(text=True)
                    ttext = [] if ttext is None else ttext
                    text  = ''.join(ttext)
                    tname = td.name

                    if tname == 'td' and td.find('b') is not None:
                        tname = 'th'

                    text  = parsetext(text)

                    tline.append([tname, text])

                if hasTable: break
                lines.append( tline )

            if hasTable:
                #print "HAS TABLE"
                continue



            # try to clean empty columns
            ctypes  = []
            lengths = []
            for line in lines:
                if len(ctypes) == 0:
                    ctypes  = [[    x[0] ] for x in line]
                    for cpos in range(len(line)):
                        try:
                            lengths.append( [len(line[cpos][1])] )
                        except:
                            lengths.append( [1                 ] )
                else:
                    for cpos in range(len(line)):
                        ctypes[ cpos].append(    line[cpos][0] )
                        try:
                            lengths[cpos].append(len(line[cpos][1]))
                        except:
                            lengths[cpos].append( 1 )

            # figure empty columns
            empties = []
            for colnum in range(len(lengths)):
                lpos = len(lengths[colnum])
                cpos = len(ctypes[ colnum])
                if lpos == cpos:
                    emp = list(set(lengths[colnum]))
                    if len(emp) == 1 and emp[0] == 0:
                        empties.append(colnum)


            #delete empty columns
            for empty in sorted(empties, reverse=True):
                for headerline in headers:
                    headerline.pop( empty )

                for line in lines:
                    #print 'poping empty'
                    line.pop( empty )


            #delete empty rows
            for linepos in range(len(lines)-1, 0, -1):
                line    = lines[linepos]
                linestr = [ str(x[1]) for x in line ]
                linestr = ''.join(linestr)
                if len(linestr) == 0:
                    #print "poping empty line", linestr
                    lines.pop(linepos)
                else:
                    #print linestr
                    pass




            # add headers
            for header in headers:
                tdata.addHeader( header )

            # get headers not inside of thead
            hlines = []
            for line in lines:
                cels     = [x[0] for x in line if x[1] != '']
                linetype = list(set(cels))
                if len(linetype) == 1 and len(cels) > 1 and linetype[0] == 'th':
                    tdata.addHeader( [ x[1] for x in line ] )
                    hlines.append(   line )

            # delete header from data
            for hline in hlines:
                lines.remove( hline )
                #print 'removin header line',hline




            # try to get row names
            ctypes  = []
            for line in lines:
                if len(ctypes) == 0:
                    ctypes  = [[ x[0] ] for x in line]
                else:
                    for cpos in range(len(line)):
                        ctypes[ cpos].append( line[cpos][0] )

            #figure row headers
            chs = []
            for colnum in range(len(ctypes)):
                cval = list(set(ctypes[colnum]))
                if len(cval) == 1 and cval[0] == 'th':
                    chs.append(colnum)
                else:
                    #print "no label", colnum, cval
                    pass


            # split lines
            for linepos in range(len(lines)):
                line = lines[linepos]
                if line[0][0] == 'th' and line[0][1] in ['Values', 'Count']:
                    lineb = [line[0]]
                    lineb.extend( parserowvalue( line[1:] ) )
                    lines[linepos] = lineb

            for line in lines:
                tdata.addLine( [x[1] for x in line], lebelsPos=chs )

            if (title, "") not in dic[ filename ]:
                dic[ filename ][ (title, "") ] = []

            dic[ filename ][ (title, "") ].append( tdata )





        for img   in soup.find_all('img', recursive=True):
            #continue
            name, title = getNameAndTitle(img)

            if name  is None: continue
            if title is None: continue

            tdata = {
                    'name' : name,
                    'title': title,
                }

            src   = parsetext( img.get('src', None) )
            if src is None: continue
            tdata['src'] = src

            ttext = img.find(text=True)
            ttext = [] if ttext is None else ttext
            text  = ''.join(ttext)
            text  = parsetext(text)

            print "parsing %s :: Image :: A name : %s" % ( filename, name  )
            print "parsing %s :: Image :: B title: %s" % ( filename, title )
            #print "parsing %s :: Image :: source : %s" % ( filename, src   )
            #print "parsing %s :: Image :: text   : %s" % ( filename, text  )
            print

            parseImgText(text, tdata)

            if (title, "") not in dic[ filename ]:
                dic[ filename ][ (title, "") ] = []

            dic[ filename ][ (title, "") ].append( tdata )

    print "DONE. exporting"

    with open(outfile, 'w') as ofhd:
        print "converting"
        jsonstr = jsonpickle.encode( dic )
        print "converted"
        print len(jsonstr)
        print "saving"
        ofhd.writelines( jsonstr.splitlines(True) )
        print "saved"

    print "finished"


def fixshittihtml(data):
    pairs = (
                ('border=0>'              , 'border="0">'                  ),
                ('width=15%>'             , 'width="15%">'                 ),
                ('border=1>'              , 'border="1">'                  ),
                ('<br>'                   , '<br/>'                        ),
                #('alternative) </br> </b>', 'alternative) </br> </b> </td>'),
                #('</td>\n</td>'           , '</td>\n</tr>'                 )
        )

    for pair in pairs:
        data = data.replace(pair[0], pair[1])

    #data = re.sub(r'bgcolor=(\S{6})>', r'bgcolor="\1">', data)
    data = re.sub(r'=(\w+?)>', r'="\1">', data)

    return data


def parseImgText(imgTxt, data):
    lines = imgTxt.split("\n")

    for line in lines:
        line = line.strip()
        if len(line) == 0 : continue
        rowname, rowvals = line.split(":")
        rowname = rowname.strip()
        rowvals = [ parsetext(x) for x in rowvals.split("\t") if len(x) != 0]
        data[ rowname ] = rowvals
        if rowname == 'Chromosome':
            data[ rowname ] = rowvals[0]


def getNameAndTitle(src):
    res = [None, None]
    a      = src.find_previous("a")
    if a is None: return res

    name  = a.get('name', None)
    name  = parsetext( name )

    if name is None     : return res
    #if name == "summary": return res

    title = None
    for tit in sorted( src.find_all_previous("b", recursive=False) , reverse=True):
        if tit.parent.name in ["small", "td", "th", "br"]:
            continue

        if tit.find_previous('a') == a:
            title = tit
            break

    if title is None: return res

    title = title.string.strip().strip(":").strip("\'")
    title = parsetext( title )

    name  = str(name)
    title = str(title)
    res   = [name, title]

    return res


def parsetext(text):
    if text is None:
        return ""

    if len(text) == 0:
        return text

    try:
        text = unicodedata.normalize('NFKD', text)
    except:
        pass

    try:
        text = text.encode('ascii', 'ignore')
    except:
        #print " error encoding text" , text
        #sys.exit(1)
        text = ""
    text = text.strip()

    #print "'%s'" % text
    if text[-1:] == "%":
        ttext = parsetext(text[:-1])

        if not isinstance(ttext, str):
            return ttext / 100
        else:
            return ttext

    try:
        text = int(text)
    except:
        try:
            text = float(text)
        except:
            ttext = text.replace(',','')
            try:
                text  = int(ttext)
            except:
                try:
                    text = float(ttext)
                except:
                    pass

    return text


def parserowvalue(rowvalue):
    if len( rowvalue ) == 1:
        if isinstance(rowvalue[0][1], str) and rowvalue[0][1].find("\t") != -1:
            rowvalue = [ [ rowvalue[0][0], [parsetext(x) for x in rowvalue[0][1].split("\t") ]] ]
    else:
        #print rowvalue
        pass

    return rowvalue



class prettytable(object):
    def __init__(self, source, name, title, ignoreblank=True):
        self.source      = source
        self.name        = name
        self.title       = title
        self.ignoreblank = ignoreblank
        self.ths         = []
        self.chs         = []
        self.tds         = []
        self.chth        = []

    def addHeader(self, thline, hasChTh=True):
        if hasChTh:
            #print 'poping chth', thline[0]
            self.chth.append( thline.pop(0) )

        #print thline

        while len( self.ths ) < len(thline):
            self.ths.append([])

        for col in range(len(thline)):
            self.ths[col].append( thline[col] )

    def addLine(self, tdline, lebelsPos=[]):
        if len(lebelsPos) == 0:
            self.tds.append( tdline )

        else:
            for pos in lebelsPos:
                ch = tdline[ pos ]
                self.chs.append( ch     )

            for pos in sorted(lebelsPos, reverse=True):
                ch = tdline.pop( pos )
                #print "poping ch", pos, ch

            if len(self.ths) > 0 and ( len(self.ths) != len(tdline) ):
                print "number of columns %d != %d number of headers" % (len(self.ths), len(tdline))
                print self.ths
                print tdline
                sys.exit(1)

            self.tds.append(     tdline )

    def pprint(self):
        for line in self.thlines:
            print "*\t".join( line )

        for linenum in range( len(self.tds) ):
            tdline = self.tds[   linenum ]

            print thline, '*\t', "\t".join(tdline)

    def add(self, data):
        src = data.source

        if len( self.ths    ) == 0:
            self.ths    = copy.deepcopy(data.ths)

            if len( data.ths    ) == 0:
                self.ths = ['+']*len(data.tds[0])

            if len( self.chth ) == 0:
                self.chth = copy.deepcopy(data.chth)

                if len( data.chth ) == 0:
                    self.chth = ['=']

        if len( data.ths    ) == 0:
            data.ths = ['+']*len(data.tds[0])

        if len( self.tds    ) == 0:
            self.tds    = copy.deepcopy( data.tds )
            for row in range( len( self.tds ) ):
                for col in range( len ( self.tds[row] ) ):
                    #print "pval",self.tds[line][col],type(self.tds[line][col])
                    val = self.tds[row][col]
                    self.tds[row][col] = stats(ignoreblank=self.ignoreblank)
                    self.tds[row][col].add( val, src=src )

        if len( self.chs ) == 0:
            self.chs = copy.deepcopy( data.chs )

        if len( self.chth ) == 0:
            self.chth = copy.deepcopy( data.chth )

        assert len(data.chs) == len(data.tds) , "data.chs (%d) != data.tds (%d)" % ( len(data.chs), len(data.tds) )
        assert len(self.chs) == len(self.tds) , "self.chs (%d) != self.tds (%d)" % ( len(self.chs), len(self.tds) )

        #print self.ths
        #print data.ths
        #
        #print self.chs
        #print data.chs

        for th_d in data.ths:
            #print "th d %s" % th_d
            if th_d not in self.ths:
                print 'unknown th',th_d
                self.ths.append( th_d )
                for tdpos in range(len(self.tds)):
                    self.tds[tdpos].append( stats(ignoreblank=self.ignoreblank) )
                #sys.exit(1)

            th_d_pos = data.ths.index( th_d )
            th_s_pos = self.ths.index( th_d )
            #print "th d %s (%dx%d)" % (th_d, th_d_pos, th_s_pos)

            for ch_d in data.chs:
                #print "th d %s (%dx%d) ch d %s" % (th_d, th_d_pos, th_s_pos, ch_d)
                if ch_d not in self.chs:
                    print 'unknown ch', ch_d
                    self.chs.append( ch_d )
                    self.tds.append( []   )
                    while len( self.tds[-1] ) != len( self.ths ):
                        self.tds[-1].append( stats(ignoreblank=self.ignoreblank) )

                    #sys.exit(1)
                ch_d_pos = data.chs.index( ch_d )
                ch_s_pos = self.chs.index( ch_d )
                #print "th d %s (%dx%d) ch d %s (%dx%d)" % (th_d, th_d_pos, th_s_pos, ch_d, ch_d_pos, ch_s_pos)

                try:
                    #print "th d %s (%dx%d) ch d %s (%dx%d)[%d+%dx%d+%d]" % (th_d, th_d_pos, th_s_pos, ch_d, ch_d_pos, ch_s_pos, len(data.tds), len(data.chs), len(self.tds), len(self.chs))
                    #print "th d %s ch d %s" % ( th_d, ch_d )
                    val = data.tds[ch_d_pos][th_d_pos]
                    dst = self.tds[ch_s_pos][th_s_pos]
                    dst.add( val, src=src )

                except:
                    print "th d %s (%dx%d) ch d %s (%dx%d)[%dx%d]" % (th_d, th_d_pos, th_s_pos, ch_d, ch_d_pos, ch_s_pos, len(data.tds), len(self.tds))
                    print "ERROR. not able to get values"
                    print data.tds
                    print self.tds

                    print len(data.tds), ch_d_pos
                    print len(self.tds), ch_s_pos

                    print data.tds[ch_d_pos]
                    print self.tds[ch_s_pos]

                    print data.tds[ch_d_pos][th_d_pos]
                    print self.tds[ch_s_pos][th_s_pos]
                    sys.exit(1)


    def tocsv(self, sep="\t", raw=False):
        res      = ""
        ththcols = []
        thcols   = []

        if len(self.ths) != 0:
            if len(self.ths[0]) ==   len(self.tds[0]) -1:
                #print "adding", len(self.ths[0]), self.ths[0]
                #print "adding", len(self.tds[0]), self.tds[0]
                #ththcols.append( '-' )
                #thcols.append(   '-' )
                pass

        else:
            ththcols.append( '-' )
            thcols.append(   '-' )
            #self.ths = ['-']*(len(self.tds[0]))
            #print "TDS" , self.tds[0]
            #print "THS" , self.ths
            #print "CHTH", self.chth



        srcst = {}
        srcs  = []
        rows  = []

        for rownum in range( len(self.tds) ):
            row     = self.tds[rownum]
            for celpos in range(len(row)):
                cel     = row[   celpos]
                if not(isinstance(cel, int) or isinstance(cel, float) or isinstance(cel, str)):
                    try:
                        statsv  = cel.getRaw()
                        #print "STATSV",statsv
                    except:
                        print "wrong format", cel
                        sys.exit(1)

                    #print "STATSV       ",statsv
                    #print "STATSV SORTED",sorted(statsv, key=lambda statsv: statsv[0])
                    for src in sorted(statsv, key=lambda statsv: statsv[0]):
                        k, v = src
                        srcst[k] = 1

        srcs = sorted(srcst.keys())


        for rownum in range( len(self.tds) ):
            row     = self.tds[rownum]
            try:
                rowname = self.chs[rownum]
            except:
                rowname = '-'

            cels    = [str(rowname)]

            for celpos in range(len(row)):
                cel     = row[   celpos]

                if isinstance(cel, int) or isinstance(cel, float) or isinstance(cel, str) :
                    cels.append( str(cel) )

                else:
                    if raw:
                        try:
                            statsv  = cel.getRaw()
                        except:
                            print "wrong format", cel
                            sys.exit(1)

                        cols = []
                        while len(cols) < len(srcs):
                            cols.append("0")
                        #print "COL LEN",len(cols)

                        for src in sorted(statsv, key=lambda statsv: statsv[0]):
                            k, v = src

                            kp   = srcs.index(k)

                            #print "  K", k, "KP",kp,"V",v

                            cols[kp] = str( v )

                        cels.extend( cols )

                    else:
                        try:
                            statsv  = cel.getFmt()
                        except:
                            print "wrong format", cel
                            sys.exit(1)
                        cols    = [ statsv[ x[0] ] for x in stats.statsKeys ]
                        #print "COLS", cols
                        cels.extend( cols )

            #print "CELS",len(cels),cels
            rows.append( sep.join( cels ) )




        for thcolpos in range(len(self.ths)):
            thline   = self.ths[ thcolpos ]

            try:
                ththcols.append( str(self.chth[thcolpos]) )
                thcols.append(   str(self.chth[thcolpos]) )

            except:
                print 'pass',self.chth
                pass


            if raw:
                for thpos in range(len(thline)):
                    thcel = thline[  thpos ]

                    ththcols.append( thcel )
                    ththcols.extend( [""]*(len(srcs)-1) )

                    thcols.extend( [ str(thcel)+" - "+str(x) for x in srcs] )

            else:
                for thpos in range(len(thline)):
                    thcel = thline[  thpos ]

                    ththcols.append( thcel )
                    ththcols.extend( [""]*(len(stats.statsKeys)-1) )

                    thcols.extend( [ str(thcel)+" - "+str(x[0]) for x in stats.statsKeys] )

        #print "THCOLS",thcols
        res += sep.join([str(x) for x in ththcols]) + "\n"
        res += sep.join([str(x) for x in thcols  ]) + "\n"
        res += "\n".join(rows) + "\n"

        return res



class stats(object):
    statsKeys = sorted([    ('mean'    , "%.12f"), ('median'  , "%.12f"    ), ('std'   , "%.12f"),
                            ('var'     , "%.12f"), ('varp'    , "%24.12f%%"),# ('minmax', "%.12f"),
                            ('min'     , "%.12f"), ('max'     , "%.12f"    ), ('size'  , "%d"   ),
                            ('mean+std', "%.12f"), ('mean-std', "%.12f"    )
                        ])

    def __init__(self, ignoreblank=True, arraytype='f', sigma=1):
        self.ignoreblank = ignoreblank
        self.data        = array.array(arraytype)
        self.srcs        = []
        self.stats       = None
        self.sigma       = sigma

    def add(self, val, src=None):
        if not isinstance(val, float):
            if isinstance(val, int):
                val = float(val)

        if isinstance(val, float):
            if src is not None:
                self.srcs.append( src )
            self.data.append( val )
            self.stats = None

        else:
            if self.ignoreblank:
                pass
            else:
                print "error adding val: ", val
                sys.exit(1)

    def bulk( self, data ):
        self.data  = array.array(self.arraytype, data)
        self.stats = None

    def mean(self):
        val = 0
        try:
            val = numpy.mean( self.data )
        except:
            pass
        return val

    def median(self):
        val = 0
        try:
            val = numpy.median( self.data )
        except:
            pass
        return val

    def std(self):
        val = 0
        try:
            val = numpy.std( self.data )
        except:
            pass
        return val

    def var(self):
        val = 0
        try:
            val = numpy.var( self.data )
        except:
            pass
        return val

    def minmax(self):
        val = 0
        try:
            val = numpy.ptp( self.data )
        except:
            pass
        return val

    def min(self):
        val = 0
        try:
            val = numpy.nanmin( self.data )
        except:
            pass
        return val

    def max(self):
        val = 0
        try:
            val = numpy.nanmax( self.data )
        except:
            pass
        return val

    def varp(self):
        val = 0
        try:
            m   = self.mean()
            v   = self.var()
            if m == 0 or v == 0:
                return 0
            val = v / m
        except:
            pass
        return val

    def meanSigmaPlus(self, sigma=0):
        mean   = self.mean()
        std    = self.std()
        lsigma = self.sigma if sigma == 0 else sigma
        #print "M", mean, 'SS', lsigma, 'S', std, 'R',mean + ( mean * ( lsigma * std))
        return mean + ( lsigma * std )

    def meanSigmaMinus(self, sigma=0):
        mean   = self.mean()
        std    = self.std()
        lsigma = self.sigma if sigma == 0 else sigma
        return mean - ( lsigma * std )

    def calc(self):
        self.stats = None
        self.stats = {
            'mean'    : self.mean(),
            'mean+std': self.meanSigmaPlus(),
            'mean-std': self.meanSigmaMinus(),
            'median'  : self.median(),
            'std'     : self.std(),
            'var'     : self.var(),
            'varp'    : self.varp(),
            #'minmax'  : self.minmax(),
            'min'     : self.min(),
            'max'     : self.max(),
            'size'    : len(self.data)
        }

    def get(self):
        if self.stats is None:
            self.calc()

        return self.stats

    def getFmt(self):
        st = self.get()

        for key in stats.statsKeys:
            st[key[0]] = key[1] % st[key[0]]

        return st

    def getSrcs(self):
        return self.srcs

    def getRaw(self):
        res = []

        for pos in range(len(self.srcs)):
            res.append( ( self.srcs[pos], self.data[pos] ) )

        return res

    def __str__(self):
        return str(self.data)

    #def __str__(self):
    #    if self.stats is None:
    #        self.calc()
    #
    #    return "mean %(mean)f median %(median)f std %(std)f var %(var)f varp %(varp)6.3f%% min %(min)f max %(max)f size %(size)d" % self.stats

    def __repr__(self):
        return str( self )

if __name__ == '__main__': main()
