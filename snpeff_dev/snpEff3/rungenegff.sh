set -xeu
ingff=$1

find genes_bam -maxdepth 1 -name '*.vcf.gz' | sort | xargs -I{} -n 1 -P 10 bash -c 'echo PARSING GFF GENE {}; bash rungene.sh '${ingff}' {}'

OUTJSON=${ingff}.json
OUTCSV=${ingff}.json.csv

if [[ ! -f "${OUTJSON}"   ]]; then
    /home/assembly/nobackup/snpeff/parsesummary_csv.py $OUTJSON ${ingff}*.snpeff.csv
fi

if [[ ! -f "${OUTJSON}"   ]]; then
    echo " error creating json"
    exit 1
fi

if [[ ! -f "${OUTCSV}"   ]]; then
    #echo $COMPILE $OUTJSON $OUTCSV
    /home/assembly/nobackup/snpeff/compilereport.py $OUTJSON $OUTCSV
fi

if [[ ! -f "${OUTCSV}"   ]]; then
    echo " error creating csv"
    exit 1
fi


#title    : Count by effects
#title    : Count by genomic region
OUTTSTV=${OUTCSV}.tstv.csv
OUTDNDS=${OUTCSV}.dnds.csv

f=${OUTCSV}
n=`grep -n "Ts/Tv : All variants" $f | head -1 | cut -d ":" -f1`
m=`cat $f | wc -l`
d=$((m-n+1))
tail -$d $f > $OUTTSTV


f=${OUTCSV}
n=`grep -n "title    : Count by effects"        $f | head -1 | cut -d ":" -f1`
m=`grep -n "title    : Count by genomic region" $f | head -1 | cut -d ":" -f1`
m=$((m-1))
d=$((m-n+1))
head -$m $f | tail -$d > $OUTDNDS
echo " DONE CREATING OUT DNDS "


f=${OUTTSTV}
n=`grep -n "Ts/Tv summary" $f | head -1 | cut -d ":" -f1`
m=`cat $f | wc -l`
d=$((m-n+1))
tail -$d $f > $OUTTSTV.summary.csv

f=${OUTDNDS}
n=`grep -n "title    : Count by effects"        $f | head -2 | tail -1 | cut -d ":" -f1`
m=`cat $f | wc -l`
d=$((m-n+1))
head -$m $f | tail -$d > $OUTDNDS.summary.csv
echo " DONE CREATING OUT DNDS SUMMARY "
