#!/usr/bin/python
import os, sys

##name     :
#Type	Count										Percent
#Type	Count - max	Count - mean	Count - mean+std	Count - mean-std	Count - median	Count - min	Count - size	Count - std	Count - var	Count - varp	Percent - max	Percent - mean	Percent - mean+std	Percent - mean-std	Percent - median	Percent - min	Percent - size	Percent - std	Percent - var	Percent - varp
#NON_SYNONYMOUS_CODING	49	19.8	34.6485689546	4.9514310454	15.5	1	50	14.8485689546	220.48	         11.135353535354%	0.2941176593	0.092097288	0.1430837253	0.0411108507	0.0785034485	0	50	0.0509864373	0.0025996168	          0.028226854973%
#SYNONYMOUS_CODING	37	18.4090909091	31.7802236241	5.0379581941	21.5	1	44	13.371132715	178.7871900826	          9.711896745230%	0.1333333254	0.068157899	0.0899264024	0.0463893957	0.0656617247	0.015625	44	0.0217685033	0.0004738677	          0.006952499182%
#SYNONYMOUS_STOP	1	1	1	1	1	1	7	0	0	          0.000000000000%	0.002079	0.0018674314	0.0019733926	0.0017614702	0.0018315	0.00175131	7	0.0001059612	1.1228E-008	          0.000006012412%
##name     :
#Type	Count																																																																																				Percent
#Type	Count - genes_in/randomgenelist_0000.lst.gff3_RF_001_SZAXPI008746-45.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_002_SZAXPI009284-57.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_003_SZAXPI009285-62.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_004_SZAXPI009286-74.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_005_SZAXPI009287-75.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_006_SZAXPI009288-79.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_007_SZAXPI009289-84.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_008_SZAXPI009290-87.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_011_SZAXPI009291-88.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_012_SZAXPI009292-89.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_013_SZAXPI009293-90.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_014_SZAXPI009294-93.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_015_SZAXPI009295-94.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_016_SZAXPI009296-95.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_017_SZAXPI009297-102.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_018_SZAXPI009298-108.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_019_SZAXPI009299-109.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_020_SZAXPI009300-113.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_021_SZAXPI009301-123.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_022_SZAXPI009302-129.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_023_SZAXPI009303-133.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_024_SZAXPI009304-136.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_025_SZAXPI009305-140.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_026_SZAXPI009306-142.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_027_SZAXPI009307-158.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_028_SZAXPI009308-166.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_029_SZAXPI009309-169.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_030_SZAXPI009310-62.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_031_SZAXPI009311-74.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_032_SZAXPI009312-75.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_033_SZAXPI009313-79.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_034_SZAXPI009314-84.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_035_SZAXPI009315-87.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_036_SZAXPI009316-88.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_037_SZAXPI008747-46.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_038_SZAXPI009317-89.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_039_SZAXPI009318-90.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_040_SZAXPI009319-93.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_041_SZAXPI009320-94.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_042_SZAXPI009321-95.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_043_SZAXPI009322-102.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_044_SZAXPI009323-108.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_045_SZAXPI009324-109.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_046_SZAXPI008748-47.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_047_SZAXPI009326-113.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_049_SZAXPI009327-123.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_051_SZAXPI009328-129.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_052_SZAXPI009329-133.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_053_SZAXPI009330-136.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_054_SZAXPI009331-140.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_055_SZAXPI009332-142.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_056_SZAXPI009333-158.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_057_SZAXPI009334-166.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_058_SZAXPI009359-46.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_059_SZAXPI009335-169.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_060_SZAXPI009336-14.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_062_SZAXPI009337-15.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_063_SZAXPI009338-16-2.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_064_SZAXPI009339-17-2.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_065_SZAXPI009340-18.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_066_SZAXPI009341-19.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_067_SZAXPI009342-21.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_068_SZAXPI009343-22-2.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_069_SZAXPI009344-23.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_070_SZAXPI008749-56.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_071_SZAXPI009345-24.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_072_SZAXPI008752-75.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_073_SZAXPI009346-25.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_074_SZAXPI008753-79.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_075_SZAXPI009347-26.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_077_SZAXPI009348-27.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_078_SZAXPI009349-30.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_088_SZAXPI009350-31.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_089_SZAXPI009351-32.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_090_SZAXPI009352-35.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_091_SZAXPI009325-56.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_093_SZAXPI009353-36.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_094_SZAXPI008750-57.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_096_SZAXPI009354-37.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_097_SZAXPI009355-39.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_102_SZAXPI009356-41.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_103_SZAXPI009357-44.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_104_SZAXPI008751-74.vcf.gz.filtered.vcf.gz.snpeff.csv	Count - genes_in/randomgenelist_0000.lst.gff3_RF_105_SZAXPI009358-45.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_001_SZAXPI008746-45.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_002_SZAXPI009284-57.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_003_SZAXPI009285-62.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_004_SZAXPI009286-74.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_005_SZAXPI009287-75.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_006_SZAXPI009288-79.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_007_SZAXPI009289-84.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_008_SZAXPI009290-87.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_011_SZAXPI009291-88.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_012_SZAXPI009292-89.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_013_SZAXPI009293-90.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_014_SZAXPI009294-93.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_015_SZAXPI009295-94.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_016_SZAXPI009296-95.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_017_SZAXPI009297-102.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_018_SZAXPI009298-108.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_019_SZAXPI009299-109.vcf.gz.filtered.vcf.gz.snpeff.csv	Percent - genes_in/randomgenelist_0000.lst.gff3_RF_020_SZAXPI009300-113.vcf.gz.filtered.vcf.gz.snpeff.csv
#NON_SYNONYMOUS_CODING	0	0	5	0	0	0	11	1	0	5	0	1	6	2	9	0	0	0	3	4	5	0	42	0	1	0	1	0	0	0	0	0	0	0	5	0	8	0	0	13	0	13	4	12	15	38	24	22	17	9	10	27	26	30	31	47	30	40	39	34	39	30	37	36	30	34	38	49	39	34	0	0	0	0	0	0	0	0	0	0	1	6	16	11	0	0	0.1315789521	0	0	0	0.2244897932	0.125	0	0.2941176593	0	0.0833333284	0	0.1538461447	0.1153846234	0	0	0
#SYNONYMOUS_CODING	0	0	3	0	0	0	6	0	0	2	0	0	2	1	7	0	0	0	0	1	3	0	34	0	0	0	0	0	0	0	0	0	0	0	2	0	4	0	0	8	0	9	5	9	8	29	23	23	5	9	6	21	22	27	28	33	30	33	34	35	35	36	30	35	34	32	34	28	32	37	0	0	0	0	0	0	0	0	0	0	0	4	5	6	0	0	0.0789473727	0	0	0	0.1224489808	0	0	0.1176470593	0	0	0.1333333254	0.0769230798	0.0897435918	0	0	0
#SYNONYMOUS_STOP	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	1	1	1	0	0	0	0	0	1	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0
##name     :

def main():
    replaces = [".lst.gff3", "randomgenelist_", ".vcf.gz.filtered.vcf.gz.snpeff.csv"]
    infile = sys.argv[1]

    data            = {}
    last_sample     = None
    last_RF_mapping = []
    RF_mapping      = []
    fields          = []
    row_names       = []

    with open( infile, 'r' ) as fhd:
        for line in fhd:
            if len(line) == 0:
                continue

            line = line.strip()
            #print line

            if len(line) == 0:
                continue

            if line[0] == "#":
                if line.startswith( "#name" ):
                    #print "CLEANING"
                    last_sample     = None
                    last_RF_mapping = []
                continue


            if line.startswith("Type" ):
                cols = line.split(",")

                if len(cols) == 2:
                    continue

                if cols[2] == "":
                    continue

                #print cols
                headers = cols[1:]

                for col in headers:
                    #print col
                    t, n = col.split(" - ")
                    fn   = os.path.basename(n)

                    name_type = 1
                    if fn == "snpEff_summary.csv":
                        name_type = 2
                        fn = n.split("/")[1]

                    #print "  TYPE",t,"NAME",n,"FN",fn

                    if n == "max":
                        break

                    for rep in replaces:
                        fn = fn.replace(rep, "")

                    fields     = fn.split("_")
                    #print "FIELDS",fields
                    sample_num = None
                    RF         = None
                    if name_type == 1:
                        sample_num = fields[0]
                        RF         = fields[2]
                    else:
                        sample_num = "global"
                        RF         = fields[1]

                    #print "    FILE",fn,"SAMPLE",sample_num,"RF",RF

                    if  last_sample is None:
                        last_sample = sample_num
                        print " SAMPLE", last_sample

                    else:
                        if last_sample != sample_num:
                            print " LAST SAMPLE != CURRENT SAMPLE"
                            sys.exit(1)


                    if (t, RF) not in last_RF_mapping:
                        last_RF_mapping.append( (t, RF) )

                    else:
                        print "DOUBLE RF", (t, RF), last_RF_mapping
                        sys.exit(1)


                    if (t, RF) not in RF_mapping:
                        RF_mapping.append( (t, RF) )

                    if last_sample not in data:
                        data[ last_sample ] = {}

                    #print
                continue


            if last_sample is not None:
                cols     = line.split(",")
                row_name = cols[0 ]
                vals     = cols[1:]

                print "  -", row_name
                if row_name not in row_names:
                    row_names.append( row_name )

                if len(vals) != len(last_RF_mapping):
                    print "  NUM COLS DO NOT MATCH", len(vals), len(last_RF_mapping)
                    print "    LINE   ", line
                    print "    ROWNAME", row_name
                    print "    VALS   ", vals
                    sys.exit(1)

                if row_name not in data[ last_sample ]:
                    data[ last_sample ][ row_name ] = {}

                for vpos in range( len(vals) ):
                    data[ last_sample ][ row_name ][ last_RF_mapping[vpos] ] = vals[vpos]

    print " exporting"
    print " row_names", row_names

    with open( infile + ".report.csv", 'w') as fhd:
        RF_mapping.sort()

        for row_name in sorted( row_names ):
            fhd.write( row_name + "\n" )
            fhd.write( "-," + ",".join( [x[0] + '_' + x[1] for x in RF_mapping] ) + "\n" )

            for sample_num in sorted(data):
                if row_name in data[ sample_num ]:
                    rfs  = data[ sample_num ][ row_name ]
                    vals = []

                    for RF in RF_mapping:
                        if RF not in rfs:
                            vals.append( "0"       )
                        else:
                            vals.append( rfs[ RF ] )

                    fhd.write( sample_num + "," + ",".join( vals ) + "\n" )

                else:
                    fhd.write( sample_num + "," + ",".join( ["0"]*len(RF_mapping) ) + "\n" )

            fhd.write( "\n\n" )




if __name__ == '__main__': main()
