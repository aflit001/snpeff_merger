REFNAME=$1
    vcf=`readlink -f $2`

    set -xeu

    P=$PWD

    bn=`basename $vcf`

    echo $bn

    if [[ ! -d "150/$bn" ]]; then
        echo "RUNNING SNPEFF FOR $bn"
        mkdir 150/$bn
        cd 150/$bn
        echo $PWD

        #java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -c $P/snpEff.config ${REFNAME} -v <(gunzip -c $vcf) > $bn.snpeff.vcf 2>log.log
        gunzip -c $vcf | java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -csvStats -c $P/snpEff.config ${REFNAME} -v > $bn.snpeff.vcf 2>log.log




        #JSON=snpEff_summary.csv.json
        #if [[ ! -f "$JSON" ]]; then
        #    $CSVPARSE $JSON ${OUTFOLDER}/RF_*/snpEff_summary.csv
        #    if [[ -f "$JSON.csv" ]]; then
        #        rm $JSON.csv
        #    fi
        #    $COMPILE $JSON $JSON.csv
        #fi
        #
        #sed -f renamer.lst ${OUTBASENAME}_RFall.json.csv > ${OUTBASENAME}_RFall.json.csv.renamed.csv





        #OUTCSV=snpEff_summary.csv
        ##title    : Count by effects
        ##title    : Count by genomic region
        #OUTTSTV=${OUTCSV}.tstv.csv
        #OUTDNDS=${OUTCSV}.dnds.csv
        #
        #
        #f=${OUTCSV}
        #n=`grep -n "Ts/Tv : All variants" $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO ALL VARIANTS IN $PWD/$f"
        #    exit 1
        #fi
        #m=`cat $f | wc -l`
        #d=$((m-n+1))
        #tail -$d $f > $OUTTSTV
        #
        #
        #f=${OUTCSV}
        #n=`grep -n "title    : Count by effects"        $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO COUNT BY EFFECTS IN $PWD/$f"
        #    exit 1
        #fi
        #m=`grep -n "title    : Count by genomic region" $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$m" ]]; then
        #    echo "NO COUNT BY GENOMIC REGION IN $PWD/$f"
        #    exit 1
        #fi
        #m=$((m-1))
        #d=$((m-n+1))
        #head -$m $f | tail -$d > $OUTDNDS
        #echo " DONE CREATING OUT DNDS $OUTDNDS"
        #
        #
        #f=${OUTTSTV}
        #n=`grep -n "Ts/Tv summary" $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO TSTV SUMMARY IN $PWD/$f"
        #    exit 1
        #fi
        #m=`cat $f | wc -l`
        #d=$((m-n+1))
        #tail -$d $f > $OUTTSTV.summary.csv
        #
        #
        #f=${OUTDNDS}
        #n=`grep -n "title    : Count by effects"        $f | head -2 | tail -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO COUNT BY EFFECTS IN $PWD/$f"
        #    exit 1
        #fi
        #m=`cat $f | wc -l`
        #d=$((m-n+1))
        #head -$m $f | tail -$d > $OUTDNDS.summary.csv
        #echo " DONE CREATING OUT DNDS SUMMARY $OUTDNDS"
        #

    else
        echo "SNPEFF ALREADY RUN FOR $bn"
    fi

    cd $P
