gff=$1

P=$PWD

bn=`basename $gff`

echo $bn

if [[ ! -d "150/$bn" ]]; then
    echo "RUNNING SNPEFF FOR $bn"
	mkdir 150/$bn
	cd 150/$bn
	echo $PWD
	java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -c $P/snpEff.config Slyc2.40 -v $gff >$bn.snpeff.vcf 2>log.log
else
    echo "SNPEFF ALREADY RUN FOR $bn"
fi

cd $P
