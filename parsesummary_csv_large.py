#!/usr/bin/python
import os
import sys
import glob

def main():
	sys.path.insert(0, '.')

	print "out ", sys.argv[1]
	glb = sys.argv.pop(2)
	print "glb ", glb
	print sys.argv
	infiles = glob.glob(glb)
	#print "infiles ", infiles
	print "len "    , len(infiles)
	sys.argv.extend( infiles )
	print "alen ",  len(sys.argv)		

	import parsesummary_csv

	parsesummary_csv.main()

if __name__ == "__main__":
	main()
