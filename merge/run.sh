set -xeu

#for F in *.vcf; do COL=$( head -50 $F | grep CHROM | cut -f 10 ); echo "$COL > $F"; if [ $COL != $F ]; then echo "YES"; sed -i 's|'$COL'|'$F'|' $F; else echo "NO"; fi; done


CURR_PWD=$PWD

BASE=/home/assembly/tomato150/programs/snpeff/merge
TABIX_PATH=$BASE/tabix-0.2.6
VCFTOOLS_PATH=$BASE/vcftools_0.1.11/perl
VCFTOOLS_PATH=$BASE/vcftools_0.1.12a/perl

FN=$CURR_PWD/merged.vcf
FN=/var/run/shm/merged/merged.vcf
GFF=$CURR_PWD/ITAG2.3_gene_models.gff3.CDS.gff3
GFF=$CURR_PWD/ITAG2.3_gene_models.gff3.gene.gff3
GFN=GENE

mkdir -p /var/run/shm/merged/

DATAFOLDER=$CURR_PWD/data
DATAFOLDER=/var/run/shm/merged/data

cd $TABIX_PATH
	export PATH=$PWD:$PATH
cd -


#for vcf in $DATAFOLDER/*.vcf; do
#	if [[ ! -f "$vcf.gz" ]]; then
		#bgzip <(gunzip -c $vcf) -c > $vcf.gz
		#bgzip $vcf -c > $vcf.gz
#	fi

#	if [[ ! -f "$vcf.gz.tbi" ]]; then
		#tabix -p vcf $vcf.gz
#	fi
#done

#exit 0

cd $VCFTOOLS_PATH
	if [[ ! -f "$FN" ]]; then
		./vcf-merge -R -t $DATAFOLDER/*.vcf.gz > $FN
	fi
cd -


./mkheadertranslator.py $CURR_PWD/names.lst $DATAFOLDER/*.vcf.gz


sed -f mkheadertranslator.py.out -i $FN




gzip -c -9 $FN > ${FN}.gz



ch=00
vcfmerger/vcffiltergff.py --gff $GFF --chrom SL2.40ch${ch} -i ${FN}.vcf -o ${FN}.${ch}.vcf.gz.$GFN.vcf

for ch in 01 02 03 04 05 06 07 08 09 10 11 12; do
	echo $ch
	vcfmerger/vcffiltergff.py --gff $GFF --chrom SL2.40ch${ch} -i ${FN}.vcf -o ${FN}.${ch}.vcf.gz.$GFN.vcf &
done

wait 1 2 3 4 5 6 7 8 9 10 11 12

tar acvf $CURR_PWD/84.merged.CDS.vcf.tar.xz ${FN}*.$GFN.vcf

for ch in 00 01 02 03 04 05 06 07 08 09 10 11 12; do
	echo $ch
	gzip -1 ${FN}.${ch}.vcf.*.vcf &
done

wait 1 2 3 4 5 6 7 8 9 10 11 12 13

#./vcf-stats ../../merged.vcf | tee ../../merged.vcf.status


#for FA in *.CDS.vcf.gz; do
#	#FA=merged.vcf.00.CDS.vcf.gz;
#	FO=$FA.translated.vcf.gz
#	if [[ ! -f $FO ]]; then
#		gzip -dc $FA | sed -f mkheadertranslator.py.out | gzip -1 > $FO
#	fi
#done
