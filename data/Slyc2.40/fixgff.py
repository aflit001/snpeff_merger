#!/usr/bin/python

import os, sys

try:
	infile = sys.argv[1]
except:
	sys.exit( 1 )

with open(infile, 'r') as fhd:
	for line in fhd:
		line = line.strip()
		if len(line) == 0  : 
			print line
			continue

		if line[0]   == "#": 
			print line
			continue

		cols = line.split("\t")

		start = int(cols[3])
		end   = int(cols[4])

		if start > end:
			cols[4] = start
			cols[3] = end

		print "\t".join([str(x) for x in cols])

