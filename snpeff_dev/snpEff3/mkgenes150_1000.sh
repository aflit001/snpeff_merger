GFFFOLDER=genes_in_1000_csv

CSVPARSE=./parsesummary_csv_large.py
COMPILE=./compilereport.py

ingff=randomgenelist


OUTJSON=${GFFFOLDER}.json
OUTCSV=${GFFFOLDER}.json.csv

if [[ ! -f "${OUTJSON}"   ]]; then
	echo "CSV PARSE"
    #$CSVPARSE $OUTJSON "$GFFFOLDER/${ingff}*.snpeff.csv"
fi

if [[ ! -f "${OUTJSON}"   ]]; then
    echo " error creating json"
    exit 1
fi

if [[ ! -f "${OUTCSV}"   ]]; then
	echo "COMPILING REPORT"
    #echo \$COMPILE \$OUTJSON \$OUTCSV
    $COMPILE $OUTJSON $OUTCSV
fi

if [[ ! -f "${OUTCSV}"   ]]; then
    echo " error creating csv"
    exit 1
fi


#title    : Count by effects
#title    : Count by genomic region
OUTTSTV=${OUTCSV}.tstv.csv
OUTDNDS=${OUTCSV}.dnds.csv


f=${OUTCSV}
n=`grep -n "Ts/Tv : All variants" $f | head -1 | cut -d ":" -f1`
m=`cat $f | wc -l`
d=$((m-n+1))
tail -$d $f > $OUTTSTV


f=${OUTCSV}
n=`grep -n "title    : Count by effects"        $f | head -1 | cut -d ":" -f1`
m=`grep -n "title    : Count by genomic region" $f | head -1 | cut -d ":" -f1`
m=$((m-1))
d=$((m-n+1))
head -$m $f | tail -$d > $OUTDNDS
echo " DONE CREATING OUT DNDS $OUTDNDS"





f=${OUTTSTV}
n=`grep -n "Ts/Tv summary" $f | head -1 | cut -d ":" -f1`
m=`cat $f | wc -l`
d=$((m-n+1))
tail -$d $f > $OUTTSTV.summary.csv

f=${OUTDNDS}
n=`grep -n "title    : Count by effects"        $f | head -2 | tail -1 | cut -d ":" -f1`
m=`cat $f | wc -l`
d=$((m-n+1))
head -$m $f | tail -$d > $OUTDNDS.summary.csv
echo " DONE CREATING OUT DNDS SUMMARY $OUTDNDS"



exit 0


find $GFFFOLDER -maxdepth 1 -name '*.gff3' | sort | xargs -I{} -n 1 -P 1 bash -c 'echo PARSING GFF DATA {}; bash rungenegff.sh {}'


cat $GFFFOLDER/*.tstv.csv             > $GFFFOLDER/tstv.csv
cat $GFFFOLDER/*.tstv.csv.summary.csv > $GFFFOLDER/tstv.csv.summary.csv


cat $GFFFOLDER/*.dnds.csv             > $GFFFOLDER/dnds.csv
cat $GFFFOLDER/*.dnds.csv.summary.csv > $GFFFOLDER/dnds.csv.summary.csv
grep -E '#name|Type,Count|Type,Count - max|SYNONYMOUS'  $GFFFOLDER/dnds.csv > $GFFFOLDER/dnds.csv.filtered.csv

cp genes_in/dnds* genes_in/tstv* .

#JSON=snpEff_summary_$bn.json
#rm $JSON
#./parsesummary_csv.py $JSON $OUTFOLDER/*/snpEff_summary.csv
#rm $JSON.csv
#./compilereport.py $JSON $JSON.csv

