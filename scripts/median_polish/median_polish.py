#!/usr/bin/python
#https://github.com/borisvish/Median-Polish
import numpy as np
import pylab
from scipy.stats import norm
import sys
import os
import copy
import argparse



#prefix=median_polish.py.lycopersicum
#
#rm *.png *.pdf *.median.*.?sv
#./median_polish.py
#
#export prefix=median_polish.py.lycopersicum_subtract;
#
#export prefix=median_polish.py.lycopersicum;
#
#rm $prefix.00.*;
#cat $prefix.??.tsv > $prefix.00.tsv;
#sed -i '/^\s*$/d' $prefix.00.tsv;
#cat $prefix.01.tsv.median.grand_effect.tsv > $prefix.00.tsv.median.grand_effect.tsv;
#for m in col_effects original residuals row_effects valids_bool valids_vals; do
#    cat $prefix.??.tsv.median.$m.tsv >> $prefix.00.tsv.median.$m.tsv;
#    sed -i '/^\s*$/d' $prefix.00.tsv.median.$m.tsv;
#done;
#
#./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv                                                        -pdf $prefix.00.tsv
#./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_snp.csv  -o ${prefix}.00.tsv.order_snp.tsv  -pdf $prefix.00.tsv
#./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_tree.csv -o ${prefix}.00.tsv.order_tree.tsv -pdf $prefix.00.tsv
#
#./median_polish.py $prefix.00.tsv
#./median_polish.py -p -xr 90 -c "0,S.lyc LA2706,1,S.lyc LA2838A,2,S.lyc PI406760,3,S.lyc LA1090,4,S.lyc EA00325,5,S.lyc EA00488,6,S.lyc EA00375,7,S.lyc EA00371,8,S.lyc LA2463,9,S.lyc LYC1969,10,S.lyc LYC1738,11,S.lyc LYC3476,12,S.lyc TR00003,13,S.lyc LYC1343,14,S.lyc LYC3306,15,S.lyc EA01155,16,S.lyc EA01049,17,S.lyc LYC3153,18,S.lyc EA03222,19,S.lyc PI129097,20,S.lyc PI272654,21,S.lyc EA00990,22,S.lyc EA00157,23,S.lyc EA02054,24,S.lyc PI303721,25,S.lyc LA4451,26,S.lyc V710029,27,S.lyc PC11029,28,S.lyc PI93302,29,S.lyc SG16,30,S.lyc EA1088,31,S.lyc PI203232,32,S.lyc PI131117,33,S.lyc LA1324,34,S.lyc PI158760,35,S.lyc LA0113,36,S.lyc LYC1410,37,S.lyc PI695588,38,S.lyc LYC2962,39,S.lyc LYC2910,40,S.lyc LYC2740,41,S.lyc TR00018,42,S.lyc EA00940,43,S.lyc TR00019,44,S.lyc EA01019,45,S.lyc TR00020,46,S.lyc EA01037,47,S.lyc TR00021,48,S.lyc TR00022,49,S.lyc TR00023,50,S.lyc EA01640,51,S.lyc LA4133,52,S.lyc LA1421,53,S.lyc LA1479" -r 0,01,92,02,143,03,209,04,275,05,342,06,390,07,457,08,522,09,591,10,657,11,712,12 median_polish.py.lycopersicum_subtract.00.tsv
#./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_snp.csv -pdf $prefix.00.tsv




#92	1	0
#51	2	92
#66	3	143
#66	4	209
#67	5	275
#48	6	342
#67	7	390
#65	8	457
#69	9	522
#66	10	591
#55	11	657
#67	12	712
#		779


class MedianPolish:
    """Fits an additive model using Tukey's median polish algorithm"""

    def __init__(self, fname, absolute=False, title=None, x_axis_name="Samples", y_axis_name="Bins", reorder=None):
        print "%20s processing:" % fname, fname
        self.fname       = fname
        self.x_axis_name = x_axis_name
        self.y_axis_name = y_axis_name
        self.reorder     = reorder
        self.rounds      = []
        
        self.get_fn_props()
        if title is None:
            self.title   = os.path.basename( self.fname ).replace( os.path.basename( sys.argv[0] ), "" ).replace( '.', ' ' ).replace( '_', ' ' )[:-4]
        else:
            self.title   = title

        print "%20s processing: title " % fname, self.title


        array = MedianPolish.csv_to_ndarray( self.fname, sep=self.sep )


        """Get numeric data from numpy ndarray to self.tbl, keep the original copy in tbl_org"""
        if isinstance(array, np.ndarray):
            self.tbl_org = self.do_reorder( array.copy() )

            if absolute:
                self.tbl_org = np.absolute( self.tbl_org )

            self.tbl     = self.tbl_org.copy()

        else:
            raise TypeError('Expected the argument to be a numpy.ndarray.')

    def get_fn_props( self ):
        if   self.fname.endswith('.csv'):
            self.sep   = ","
            self.ext   = ".csv"

        elif self.fname.endswith('.tsv'):
            self.sep   = "\t"
            self.ext   = ".tsv"

        else:
            print "unknown file type"
            sys.exit( 1 )

    @staticmethod
    def csv_to_ndarray(fname, sep=","):
        """
        Utility method for loading ndarray from .csv file
        """
        try:
            return np.genfromtxt(fname, delimiter=sep)
        except Exception, e:
            print "Error loading file %s:" % fname
            raise

    def median_polish(self, max_iterations=100, method='median', eps=0.01, pval=0.025, intermediates=False ):
        """
            Implements Tukey's median polish alghoritm for additive models
            method - default is median, alternative is mean. That would give us result equal ANOVA.
        """
        self.method         = method
        self.max_iterration = max_iterations
        self.eps            = eps
        self.pval           = pval
        self.grand_effect   = 0
        self.row_effects    = np.zeros(shape=self.tbl.shape[0])
        self.col_effects    = np.zeros(shape=self.tbl.shape[1])
        median_row_effects  = 0
        median_col_effects  = 0
        iternum             = 0
        self.sm             = 0
        self.sm_delta       = sys.maxint
        prev_sm             = -sys.maxint - 1
        func                = np.median

        if method == 'average':
            func = np.average

        print "method", method

        while ((iternum < max_iterations) & (self.sm_delta >= eps)):
            row_medians         = func( self.tbl  , 1 )
            self.row_effects   += row_medians
            median_row_effects  = func( self.row_effects )

            self.grand_effect  += median_row_effects
            self.row_effects   -= median_row_effects
            self.tbl           -= row_medians[:,np.newaxis]

            col_medians         = func( self.tbl,0 )
            self.col_effects   += col_medians
            median_col_effects  = func( self.col_effects )

            self.tbl           -= col_medians
            self.grand_effect  += median_col_effects

            self.sm             = np.sum(np.absolute(self.tbl))
            self.sm_delta       = abs(self.sm - prev_sm)
            print "method %7s #%3d prev sm %16.4f sm %16.4f eps %5.4f diff %16.4f valid %r" % ( method, iternum, prev_sm, self.sm, eps, self.sm_delta, (self.sm_delta < eps))
            iternum            += 1
            prev_sm             = self.sm

            if intermediates:
                self.getValid()
                tables, graphs = self.get_vars()
                self.rounds.append([copy.deepcopy(tables), copy.deepcopy(graphs)])
                #np.savetxt(str(intermediates)+".median.valids_vals.round.%03d.tsv" % iternum, self.res_valid_tbl, delimiter="\t")
                #np.savetxt(str(intermediates)+".median.valids_bool.round.%03d.tsv" % iternum, self.res_valid    , delimiter="\t")

        self.getValid()

    def getValid(self):
        self.threshold          = abs(norm.ppf(self.pval))
        #print "threshold", threshold

        self.res_mean           = np.average( self.tbl )
        self.res_stddev         = np.std(     self.tbl, dtype=np.float32 )

        #print "mean", res_mean, "stddev", res_stddev

        #res_mean_tbl       = np.ones((self.tbl.shape[0],self.tbl.shape[1])) * res_mean
        #res_stddev_tbl     = np.ones((self.tbl.shape[0],self.tbl.shape[1])) * res_stddev
        #res_valid_tbl      = self.tbl      - res_mean
        #res_valid_tbl      = res_valid_tbl / res_stddev_tbl

        #print "table"
        #print self.tbl

        self.res_valid_tbl      = self.tbl      - self.res_mean
        #print "table - mean"
        #print res_valid_tbl

        self.res_valid_tbl      = self.res_valid_tbl / self.res_stddev
        #print "(table - mean) / stddev"
        #print res_valid_tbl

        self.res_valid          = np.asarray( self.res_valid_tbl.copy() )

        #print "mean tbl\n"  , res_mean_tbl
        #print "stddev tbl\n", res_stddev_tbl
        #print "valid tbl\n" , res_valid_tbl

        for i in range(self.res_valid_tbl.shape[0]):
            for j in range(self.res_valid_tbl.shape[1]):
                if abs(self.res_valid[i,j]) > self.threshold:
                    if self.res_valid[i,j] > 0:
                        self.res_valid[i,j] = 1
                    else:
                        self.res_valid[i,j] = 1
                else:
                    self.res_valid[i,j] = 0

    def do_reorder(self, tbl):
        if self.reorder is not None:
            print "   reordering", self.reorder

            if len( self.reorder ) != tbl.shape[1]:
                print "asked to reorder. received %d colums. table has %d colums." % (len( self.reorder ), tbl.shape[1])
                print self.reorder
                sys.exit(1)

            else:
                #http://stackoverflow.com/questions/7891247/numpy-reorder-array-by-specified-values
                tbl = tbl[:,self.reorder]
                return tbl

        else:
            print "   not reordering"
            return tbl

    def get(self):
        return self.grand_effect, self.col_effects, self.row_effects , self.tbl, self.tbl_org, self.res_valid_tbl, self.res_valid

    def set(self, basename, method, pval):
        self.method    = method
        self.pval      = pval

        tables, graphs = self.get_vars()

        for var in graphs:
            valN     = var[0]
            name     = var[1]

            bn       = "%s.%s.%s%s"    % ( basename, self.method, name, self.ext )

            print "  reading", bn

            if not os.path.exists( bn ):
                print "file %s does not exists" % bn
                sys.exit( 1 )

            with open(bn, 'r') as fhd:
                val = MedianPolish.csv_to_ndarray( bn, sep=self.sep )
                #print "val", val
                self.__dict__[ valN ] = self.do_reorder( val )
                #print "val", val



        for var in tables:
            valN     = var[0]
            name     = var[1]
            eType    = var[2]

            bn       = "%s.%s.%s%s"    % ( basename, self.method, name, self.ext )

            print "  reading", bn

            if not os.path.exists( bn ):
                print "file %s does not exists" % bn
                sys.exit( 1 )

            with open(bn, 'r') as fhd:
                if   eType == 'list':
                    val = MedianPolish.csv_to_ndarray( bn, sep=self.sep )
                    self.__dict__[ valN ] = val
                    print "val", val

                elif eType == 'float':
                    ln  = fhd.readlines()[0]
                    val = float( ln )
                    self.__dict__[ valN ] = val
                    print "val", val

    def print_vals( self ):
        seqname = "%20s %s " % ( self.fname, self.method )

        vals =  [
                    [ "Grand Effect"       , self.grand_effect ],
                    [ "Column Effects"     , self.col_effects  ],
                    [ "Row Effects"        , self.row_effects  ],
                    [ "Original Table"     , self.tbl_org       ],
                    [ "Table of Residuals" , self.tbl           ],
                    [ "Valids Values Table", self.res_valid_tbl ],
                    [ "Valids Bool Table"  , self.res_valid     ],
                ]

        for name, val in vals:
            print "%s ----- %20s -----" % ( seqname, name ), "\n", val

        print "-"*100

    def mk_graph(self, val, name, name_ptr, do_scale=None, gpos=None, font_size_ttl=18, font_size_ax=16, font_size_lbl=18, x_rotate=0, row_names=[], col_names=[] ):
        x_size   = val.shape[1]
        y_size   = val.shape[0]

        print "exporting %s. creating image x %d y %d" % ( name, x_size, y_size )


        if x_size > 10:
            x_step = int(x_size/10)

        else:
            x_step = 1


        if y_size > 10:
            y_step = int(y_size/10)

        else:
            y_step = 1


        print "exporting %s. creating image x step %d y step %d" % ( name, x_step, y_step )


        sub = None
        if gpos is not None:
            sub = pylab.subplot( gpos )

        pylab.pcolormesh(  val   , cmap='binary' )
        #pylab.pcolormesh(  val   , cmap='binary', rasterized=True )
        pylab.xlim(        0     , val.shape[1]  )
        pylab.ylim(        y_size, 0             )
        pylab.tick_params( direction='out'       )

        cbar = pylab.colorbar( orientation='horizontal', drawedges=False, boundaries=None )

        if do_scale is not None:
            if do_scale is not True:
                cbar.set_label( do_scale, size=font_size_ax )

                if font_size_ax is not None:
                    cbar.ax.tick_params( labelsize=font_size_ax )
        else:
            cbar.set_alpha(      0.0 )
            cbar.set_ticks(      []  )
            cbar.set_ticklabels( []  )
            cbar.update_ticks()
            cbar.outline.set_color(     'white' )
            cbar.outline.set_linewidth( 0       )
            cbar.outline.set_visible(   False   )
            cbar.outline.set_alpha(     0       )
            cbar.draw_all()



        if font_size_ttl is not None:
            #pylab.title(       "%s - %s - %s" % ( self.title, self.method, name_ptr ), fontsize=font_size_ttl, linespacing=10 )
            pylab.title(       "%s - %s - %s" % ( self.title, self.method, name_ptr ), fontsize=font_size_ttl, y=1.04 )
            #if gpos is not None:
            #    pylab.text(0.5, 1.08, "%s - %s - %s" % ( self.title, self.method, name_ptr ), fontsize=font_size_ttl, horizontalalignment='center', transform=sub.transAxes)
            #else:
            #    pylab.title(       "%s - %s - %s" % ( self.title, self.method, name_ptr ) )
        else:
            pylab.title(       "%s - %s - %s" % ( self.title, self.method, name_ptr ) )


        c_poses = np.arange( 0, x_size  , x_step )
        c_names = range(     1, x_size+1, x_step )
        if len(col_names) > 0:
            c_poses = col_names[0]
            c_names = col_names[1]

            #print "custom col names", c_poses, c_names



        r_poses = np.arange( 0, y_size  , y_step )
        r_names = range(     1, y_size+1, y_step )
        if len(row_names) > 0:
            r_poses = row_names[0]
            r_names = row_names[1]
            #print "custom row names", r_poses, r_names

        if font_size_ax is not None:
            pylab.xticks(      c_poses, c_names, fontsize=font_size_ax, rotation=x_rotate )
            pylab.yticks(      r_poses, r_names, fontsize=font_size_ax )

        else:
            pylab.xticks(      c_poses, c_names, rotation=x_rotate )
            pylab.yticks(      r_poses, r_names )



        if font_size_lbl is not None:
            pylab.xlabel(      self.x_axis_name, fontsize=font_size_lbl )
            pylab.ylabel(      self.y_axis_name, fontsize=font_size_lbl )

        else:
            pylab.xlabel(      self.x_axis_name )
            pylab.ylabel(      self.y_axis_name )

    def get_vars(self):
        tables =    [
                        [ "grand_effect", "grand_effect", 'float', copy.deepcopy(self.grand_effect) ],
                        [ "col_effects" , "col_effects" , 'list' , copy.deepcopy(self.col_effects ) ],
                        [ "row_effects" , "row_effects" , 'list' , copy.deepcopy(self.row_effects ) ],
                        [ "sm"          , "sm"          , 'float', copy.deepcopy(self.sm          ) ],
                        [ "sm_delta"    , "sm_delta"    , 'float', copy.deepcopy(self.sm_delta    ) ],
                        [ "pval"        , "pval"        , 'float', copy.deepcopy(self.pval        ) ],
                        [ "threshold"   , "threshold"   , 'float', copy.deepcopy(self.threshold   ) ],
                        [ "res_mean"    , "res_mean"    , 'float', copy.deepcopy(self.res_mean    ) ],
                        [ "res_stddev"  , "res_stddev"  , 'float', copy.deepcopy(self.res_stddev  ) ],
                    ]

        #		         class property     file name      title                                                    scale name       position  position clean
        graphs =    [
                        [ "tbl_org"      , "original"   , "Raw Values"                                           , "Number of SNPs", 141,     121  , copy.deepcopy(self.tbl_org      ) ],
                        [ "tbl"          , "residuals"  , "Residuals"                                            , ""              , 142,     None , copy.deepcopy(self.tbl          ) ],
                        [ "res_valid_tbl", "valids_vals", "Residuals Norm"                                       , "Sigmas"        , 143,     None , copy.deepcopy(self.res_valid_tbl) ],
                        [ "res_valid"    , "valids_bool", "Residuals Valids ( Alpha = " + str( self.pval ) + " )", None            , 144,     122  , copy.deepcopy(self.res_valid    ) ]
                    ]

        return tables, graphs

    def export_tables(self, tables, graphs, basename):
        gbn = "%s.%s.%s%s"    % ( basename, self.method, 'global', self.ext )
        print 'Saving global %s' % gbn
        
        with open(gbn, 'w') as gbhd:
            gbhd.write('\n%s\n%s\n'  % ('method'   ,self.method))
        
            for var in tables:
                valN     = var[0]
                name     = var[1]
                eType    = var[2]
                val      = var[3]
                #val      = self.__dict__[ valN ]
    
                bn = "%s.%s.%s%s"    % ( basename, self.method, name, self.ext )
    
                print "exporting table %s" % bn
    
                gbhd.write('\n%s\n' % name)
                if   eType == 'list':
                    np.savetxt( bn  , val, delimiter=self.sep )
                    np.savetxt( gbhd, val, delimiter=self.sep )
    
                elif eType == 'float':
                    with open(bn, 'w') as fhd:
                        fhd.write( str(val))
                        gbhd.write(str(val))
                        
                gbhd.write('\n')
    
            for var in graphs:
                valN      = var[0] #class property
                name      = var[1] #file name
                #name_ptr  = var[2] #title
                #do_scale  = var[3] #scale name
                #position  = var[4] # position
                #positionC = var[5] # position clean
                val       = var[6] # val
                #print "VAL", valN, val
                #val      = self.__dict__[ valN ]
    
    
                bn = "%s.%s.%s%s"    % ( basename, self.method, name, self.ext )
    
                print "exporting graph %s" % bn
                sys.stdout.flush()
                
                gbhd.write( '\n%s\n' % name               )
                np.savetxt( bn  , val, delimiter=self.sep )
                np.savetxt( gbhd, val, delimiter=self.sep )
                gbhd.write( '\n'                          )

    def export_graph(self, graphs, basename, epitope=None, font_size_ttl=18, font_size_ax=16,font_size_lbl=18, sz=14, szw=14, szh=14, DPI=1200, x_rotate=0, row_names=[], col_names=[], do_pdf=False ):
        #szw = sz*3*3
        #szh = sz*3
        gcf = pylab.gcf()
        gcf.set_size_inches( ( szw, szh ) )
        gcf.set_dpi(           DPI        )

        do_merge = len([x for x in graphs if x[4] is not None]) > 0

        p = 0
        for var in graphs:
            p        += 1
            valN      = var[0]
            name      = var[1]
            name_ptr  = var[2]
            do_scale  = var[3]
            gpos      = var[4]


            if ( do_merge ) and ( gpos is None ):
                continue

            val      = self.__dict__[ valN ]

            self.mk_graph( val, "merged %d/%d" % ( p, len(graphs) ), name_ptr, do_scale, gpos=gpos, font_size_ttl=font_size_ttl, font_size_ax=font_size_ax, font_size_lbl=font_size_lbl, x_rotate=x_rotate, row_names=row_names, col_names=col_names )
            #pylab.show()

            if not do_merge:
                bn  = "%s.%s.%s%s"    % ( basename, self.method, name, self.ext )

                print "Image is %i x %i Image"%(gcf.get_dpi()*gcf.get_size_inches()[0], gcf.get_dpi()*gcf.get_size_inches()[1])
                print "  Exporting PNG", bn + ".png"
                pylab.savefig( bn + ".png" )
                if do_pdf:
                    print "  Exporting PDF", bn + ".pdf"
                    pylab.savefig( bn + ".pdf" )
                print "  Cleaning"
                pylab.clf()

        if do_merge:
            bn  = "%s.%s.%s%s"    % ( basename, self.method, epitope, self.ext )

            print "Image is %i x %i Image"%(gcf.get_dpi()*gcf.get_size_inches()[0], gcf.get_dpi()*gcf.get_size_inches()[1])
            print "  Exporting PNG", bn + ".png"
            pylab.savefig( bn + ".png" )
            if do_pdf:
                print "  Exporting PDF", bn + ".pdf"
                pylab.savefig( bn + ".pdf" )
            print "  Cleaning"
            pylab.clf()

    def export( self, basename, font_size_ttl=18, font_size_ax=16, font_size_lbl=18, sz=14, DPI=1200, x_rotate=0, row_names=[], col_names=[], do_pdf=False, do_image=True, do_tables=True ):
        if len(self.rounds) > 0:
            for r, v in enumerate(self.rounds):
                tables, graphs = v
                self._export( basename + ('_round_%d'%r), tables, graphs, font_size_ttl=font_size_ttl, font_size_ax=font_size_ax, font_size_lbl=font_size_lbl, sz=sz, DPI=DPI, x_rotate=x_rotate, row_names=row_names, col_names=col_names, do_pdf=do_pdf, do_image=do_image, do_tables=do_tables)

        tables, graphs = self.get_vars()
        
        self._export( basename, tables, graphs, font_size_ttl=font_size_ttl, font_size_ax=font_size_ax, font_size_lbl=font_size_lbl, sz=sz, DPI=DPI, x_rotate=x_rotate, row_names=row_names, col_names=col_names, do_pdf=do_pdf, do_image=do_image, do_tables=do_tables)

    def _export(self, basename, tables, graphs, font_size_ttl=18, font_size_ax=16, font_size_lbl=18, sz=14, DPI=1200, x_rotate=0, row_names=[], col_names=[], do_pdf=False, do_image=True, do_tables=True):   
        if do_tables:
            self.export_tables(tables, graphs, basename)
            
        else:
            print "SKIPPING EXPORTING TABLES"

        num_imgs = len([x for x in graphs if x[4] is not None])
        #print graphs, num_imgs



        if do_image:
            if ( self.reorder is not None ) and ( len( self.reorder ) > 0 ):
                if len( self.reorder ) != len( col_names[1] ):
                    print "wrong number of reorder columns. %d vs %d" % ( len( self.reorder ), len( col_names[1] ) )
                    sys.exit(1)

                else:
                    col_names_tmp = list( col_names[1] )
                    for p, c in enumerate( col_names_tmp ):
                        col_names[1][ p ] = col_names_tmp[ self.reorder[p] ]
                    print col_names

            self.export_graph(graphs, basename, epitope="merged"      , font_size_ttl=font_size_ttl, font_size_ax=font_size_ax, font_size_lbl=font_size_lbl, sz=sz, szw=sz*(num_imgs+0), szh=sz, DPI=DPI, x_rotate=x_rotate, row_names=row_names, col_names=col_names, do_pdf=do_pdf)

            for g in xrange(len(graphs)):
                graphs[g][4] = graphs[g][5]

            num_imgs = len([x for x in graphs if x[4] is not None])
            #print graphs, num_imgs

            self.export_graph(graphs, basename, epitope="merged_clean", font_size_ttl=font_size_ttl, font_size_ax=font_size_ax, font_size_lbl=font_size_lbl, sz=sz, szw=sz*(num_imgs+0), szh=sz, DPI=DPI, x_rotate=x_rotate, row_names=row_names, col_names=col_names, do_pdf=do_pdf)


            for g in xrange(len(graphs)):
                graphs[g][4] = None

            num_imgs = len([x for x in graphs if x[4] is not None])
            #print graphs, num_imgs

            self.export_graph(graphs, basename,                         font_size_ttl=font_size_ttl         , font_size_ax=font_size_ax         , font_size_lbl=font_size_lbl         , sz=sz, szw=sz                  , szh=sz         , DPI=DPI, x_rotate=x_rotate, row_names=row_names, col_names=col_names, do_pdf=do_pdf)
        else:
            print "SKIPPING EXPORTING IMAGES"







def runDemo( args ):
    # Example and data on volume of rubber taken from chapter 6 of
    # William N. Venables and Brian D. Ripley (2002). Statistics Complements to Modern Applied Statistics with S, ISBN 0-387-95457-0.
    #data_file='ripley.csv'
    #arr = MedianPolish.csv_to_ndarray(data_file) * 10000

    # Example and data on Arisona state temperature taken from
    # Chapter 10 of Tukey, John W (1977). Exploratory Data Analysis. Addison-Wesley. ISBN 0-201-07616-0.

    data_files = [
            "arizona.csv", "ripley.csv", "youtube.csv", "solanum.tsv",
            "lycopersicum.01.tsv", "lycopersicum.02.tsv", "lycopersicum.03.tsv", "lycopersicum.04.tsv",
            "lycopersicum.05.tsv", "lycopersicum.06.tsv", "lycopersicum.07.tsv", "lycopersicum.08.tsv",
            "lycopersicum.09.tsv", "lycopersicum.10.tsv", "lycopersicum.11.tsv", "lycopersicum.12.tsv",
            "lycopersicum.tsv",
            "lycopersicum_subtract.01.tsv", "lycopersicum_subtract.02.tsv", "lycopersicum_subtract.03.tsv", "lycopersicum_subtract.04.tsv",
            "lycopersicum_subtract.05.tsv", "lycopersicum_subtract.06.tsv", "lycopersicum_subtract.07.tsv", "lycopersicum_subtract.08.tsv",
            "lycopersicum_subtract.09.tsv", "lycopersicum_subtract.10.tsv", "lycopersicum_subtract.11.tsv", "lycopersicum_subtract.12.tsv",
            "lycopersicum_subtract.tsv"
            ]

    #data_files = [    "youtube.csv"]

    for filename in data_files:
        print "\n\n\n"
        data_file = sys.argv[0] + "." + filename

        print "ITER", args.inter
        if str(args.inter) != str(False):
            args.inter = data_file
            print "ITER", args.inter

        mp        = MedianPolish( data_file, absolute=args.absolute, x_axis_name=args.x_axis_name, y_axis_name=args.y_axis_name, reorder=args.reorder )

        mp.median_polish( max_iterations=args.max_iter, method=args.method, eps=args.eps, pval=args.pval, intermediates=args.inter )

        #mp.print_vals()

        mp.export( data_file, font_size_ttl=args.font_size_ttl, font_size_ax=args.font_size_ax, font_size_lbl=args.font_size_lbl, sz=args.size, DPI=args.DPI, x_rotate=args.x_rotate, row_names=args.row_names, col_names=args.col_names, do_pdf=args.pdf, do_image=args.no_image, do_tables=args.no_tables )



def readFiles( args ):
    for data_file in args.infiles:
        print data_file

        mp        = MedianPolish( data_file, absolute=args.absolute, title=args.title, x_axis_name=args.x_axis_name, y_axis_name=args.y_axis_name, reorder=args.reorder )

        if args.print_only:
            mp.set( data_file, args.method, args.pval )

        else:
            mp.median_polish( max_iterations=args.max_iter, method=args.method, eps=args.eps, pval=args.pval, intermediates=args.inter )

        #mp.print_vals()

        out_file = data_file
        if args.out_file is not None:
            print "OUTPUT FILE", args.out_file
            out_file = args.out_file

        mp.export( out_file, font_size_ttl=args.font_size_ttl, font_size_ax=args.font_size_ax, font_size_lbl=args.font_size_lbl, sz=args.size, DPI=args.DPI, x_rotate=args.x_rotate, row_names=args.row_names, col_names=args.col_names, do_pdf=args.pdf, do_image=args.no_image, do_tables=args.no_tables )



def main():
    parser = argparse.ArgumentParser(description='Parse file')

    parser.add_argument(          'infiles'        ,                       type=str  , nargs=argparse.REMAINDER,                    help='input files')
    parser.add_argument( '-o'   , '--out_file'     , metavar='OUT'       , type=str  , nargs='?'               , default=None     , help='output file prefix')
    parser.add_argument( '-m'   , '--method'       , metavar='M'         , type=str  , nargs='?'               , default='median' , help='method', choices=['median', 'average'])
    parser.add_argument( '-xn'  , '--x_axis_name'  , metavar='XN'        , type=str  , nargs='?'               , default='Samples', help='x axis name')
    parser.add_argument( '-yn'  , '--y_axis_name'  , metavar='YN'        , type=str  , nargs='?'               , default='Bins'   , help='y axis name')
    parser.add_argument( '-xr'  , '--x_rotate'     , metavar='XR'        , type=int  , nargs='?'               , default=0        , help='x rotate axis')
    parser.add_argument( '-r'   , '--row_names'    , metavar='RN'        , type=str  , nargs='?'               , default=None     , help='row names. comma separated pos,name')
    parser.add_argument( '-c'   , '--col_names'    , metavar='CN'        , type=str  , nargs='?'               , default=None     , help='col names. comma separated names')
    parser.add_argument( '-re'  , '--reorder'      , metavar='RE'        , type=str  , nargs='?'               , default=None     , help='reorder columns. comma separated desired column order')
    parser.add_argument( '-rf'  , '--row_names_f'  , metavar='RNf'       , type=str  , nargs='?'               , default=None     , help='row names csv file')
    parser.add_argument( '-cf'  , '--col_names_f'  , metavar='CNf'       , type=str  , nargs='?'               , default=None     , help='col names csv file')
    parser.add_argument( '-ref' , '--reorder_f'    , metavar='RNf'       , type=str  , nargs='?'               , default=None     , help='reorder columns. file with desired column order, one per line')
    parser.add_argument( '-T'   , '--title'        , metavar='T'         , type=str  , nargs='?'               , default=None     , help='graph title')
    parser.add_argument( '-v'   , '--pval'         , metavar='P'         , type=float, nargs='?'               , default=0.025    , help='p-value')
    parser.add_argument( '-e'   , '--eps'          , metavar='E'         , type=float, nargs='?'               , default=0.001    , help='EPS. min diff between residuals')
    parser.add_argument( '-fst' , '--font_size_ttl', metavar='fst'       , type=int  , nargs='?'               , default=18       , help='font size title')
    parser.add_argument( '-fsa' , '--font_size_ax' , metavar='fsa'       , type=int  , nargs='?'               , default=16       , help='font size axis')
    parser.add_argument( '-fsl' , '--font_size_lbl', metavar='fsl'       , type=int  , nargs='?'               , default=18       , help='font size label')
    parser.add_argument( '-mi'  , '--max_iter'     , metavar='mx'        , type=int  , nargs='?'               , default=100      , help='max iterations')
    parser.add_argument( '-s'   , '--size'         , metavar='S'         , type=int  , nargs='?'               , default=14       , help='size (in) of image')
    parser.add_argument( '-D'   , '--DPI'          , metavar='DPI'       , type=int  , nargs='?'               , default=1200     , help='Image DPI')
    parser.add_argument( '-t'   , '--inter'        , action="store_true" ,                                                          help='save intermediate esiduals')
    parser.add_argument( '-a'   , '--absolute'     , action="store_true" ,                                                          help='use absolute values')
    parser.add_argument( '-p'   , '--print_only'   , action="store_true" ,                                                          help='only re print graph')
    parser.add_argument( '-pdf' ,                    action="store_true" ,                                                          help='create PDF')
    parser.add_argument(          '--no_image'     , action="store_false",                                                          help='do not export images, only tables')
    parser.add_argument(          '--no_tables'    , action="store_false",                                                          help='do not export tables, only images')

    args = parser.parse_args()

    print args

    if args.row_names is not None:
        rows = args.row_names.split(',')
        args.row_names = [[],[]]
        for r in range(0, len(rows), 2):
            args.row_names[0].append( int(rows[r+0]) )
            args.row_names[1].append(     rows[r+1]  )
    else:
        args.row_names = []
        if args.row_names_f is not None:
            args.row_names = [[],[]]
            with open(args.row_names_f, 'r') as fhd:
                for row in fhd:
                    row = row.strip()
                    if len( row ) == 0:
                        continue
                    if row[0] == "#":
                        continue
                    cols = row.split(',')
                    args.row_names[0].append( int(cols[0]) )
                    args.row_names[1].append(     cols[1]  )



    if args.col_names is not None:
        cols = args.col_names.split(',')
        args.col_names = [[],[]]
        for p, c in enumerate(cols):
            print '%d %s' % (p, c)
            args.col_names[0].append( p )
            args.col_names[1].append( c )
        print "col names", args.col_names

    else:
        args.col_names = []
        if args.col_names_f is not None:
            args.col_names = [[],[]]
            with open(args.col_names_f, 'r') as fhd:
                for row in fhd:
                    row = row.strip()
                    if len( row ) == 0:
                        continue
                    if row[0] == "#":
                        continue
                    cols = row.split(',')
                    args.col_names[0].append( len(args.col_names[1]) )
                    args.col_names[1].append(     cols[0]  )
            print "col names", args.col_names



    if args.reorder is not None:
        print "reorder not none"
        reorder      = args.reorder.split(',')
        args.reorder = [ int(x) for x in reorder ]

    else:
        if args.reorder_f is not None:
            print "reorder file not none"
            args.reorder = []

            with open(args.reorder_f, 'r') as fhd:
                for row in fhd:
                    row = row.strip()
                    if len( row ) == 0:
                        continue
                    if row[0] == "#":
                        continue
                    #print row
                    pos = row.split(',')[0]
                    args.reorder.append( int(pos) )
            print args.reorder

        else:
            print "no reorder"

    #print args.row_names
    #print args.col_names
    #quit()

    if len( args.infiles ) > 0:
        readFiles( args )

    else:
        runDemo( args )


if __name__ == "__main__":
    main()
