#!/bin/bash

FILTER=$PWD/vcffiltergff.py
CSVPARSE=$PWD/parsesummary_csv.py
COMPILE=$PWD/compilereport.py

REFNAME=Slyc2.40

GENEMODELSPATH=ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG2.3_release/
GENEMODELSFILE=ITAG2.3_gene_models.gff3

#GENEMODELSPATH=ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG2.4_release/
#GENEMODELSFILE=ITAG2.4_gene_models.gff3

REFFASTAPATH=ftp://ftp.solgenomics.net/tomato_genome/assembly/build_2.40/
REFFASTAFILE=S_lycopersicum_chromosomes.2.40.fa.gz

PROTFASTAPATH=ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG2.3_release/
PROTFASTAFILE=ITAG2.3_proteins.fasta

#PROTFASTAPATH=ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG2.4_release/
#PROTFASTAFILE=ITAG2.4_proteins.fasta

DATAFOLDER=data
DATAIN=datain
OUTFOLDER=150
OUTBASENAME=snpEff_${OUTFOLDER}_summary

DOFIX=0
DOSNPEFF=1
DOMERGE=1




echo "FILTER         $FILTER"
echo "CSVPARSE       $CSVPARSE"
echo "COMPILE        $COMPILE"

echo "REFNAME        $REFNAME"

echo "GENEMODELSPATH $GENEMODELSPATH"
echo "GENEMODELSFILE $GENEMODELSFILE"

echo "REFFASTAPATH   $REFFASTAPATH"
echo "REFFASTAFILE   $REFFASTAFILE"

echo "PROTFASTAPATH  $PROTFASTAPATH"
echo "PROTFASTAFILE  $PROTFASTAFILE"

echo "DATAFOLDER     $DATAFOLDER"
echo "DATAIN         $DATAIN"
echo "OUTFOLDER      $OUTFOLDER"
echo "OUTBASENAME    $OUTBASENAME"

echo "DOFIX          $DOFIX"
echo "DOSNPEFF       $DOSNPEFF"
echo "DOMERGE        $DOMERGE"




if [[ ! -d "$DATAFOLDER" ]]; then
    mkdir $DATAFOLDER
fi
cd    $DATAFOLDER

if [[ ! -d "${REFNAME}" ]]; then
    mkdir ${REFNAME}
fi

cd    ${REFNAME}

if [[ ! -f "${GENEMODELSFILE}" ]]; then
    wget ${GENEMODELSPATH}/${GENEMODELSFILE}
fi

if [[ ! -f "$REFFASTAFILE" ]]; then
    #wget ftp://ftp.solgenomics.net/tomato_genome/assembly/build_2.40/S_lycopersicum_chromosomes.2.40.fa.gz
    wget $REFFASTAPATH/$REFFASTAFILE
fi

if [[ ! -f "$PROTFASTAFILE" ]]; then
    wget $PROTFASTAPATH/$PROTFASTAFILE
fi


set -xeu



if [[ "$DOFIX" ]]; then
    if [[ ! -f "fixgff.py" ]]; then
        PSTR=<<'HERE'
        #!/usr/bin/python

        import os, sys

        try:
                infile = sys.argv[1]
        except:
                sys.exit( 1 )

        with open(infile, 'r') as fhd:
                for line in fhd:
                        line = line.strip()
                        if len(line) == 0  :
                                print line
                                continue

                        if line[0]   == "#":
                                print line
                                continue

                        cols = line.split("\t")

                        start = int(cols[3])
                        end   = int(cols[4])

                        if start > end:
                                cols[4] = start
                                cols[3] = end

                        print "\t".join([str(x) for x in cols])
HERE
        echo $PSTR > fixgff.py
    fi


    REFFASTAFILEFIXED=$REFFASTAFILE.fixed.gff3
    REFFASTAFILEFIXEDGZ=$REFFASTAFILEFIXED.gz
    if [[ ! -f "${REFFASTAFILE}.fixed.gff3" ]]; then
        python fixgff.py $REFFASTAFILE > $REFFASTAFILEFIXED
    fi

    if [[ ! -f "$REFFASTAFILE.fixed.gff3.gz" ]]; then
        gzip -c -1 $REFFASTAFILEFIXED > $REFFASTAFILEFIXEDGZ
    fi

    if [[ ! -f "sequences.fa.gz" ]]; then
        ln -s $REFFASTAFILE        sequences.fa.gz
    fi

    if [[ ! -f "genes.gff" ]]; then
        ln -s $REFFASTAFILEFIXED   genes.gff
    fi

    if [[ ! -f "genes.gff.gz" ]]; then
        ln -s $REFFASTAFILEFIXEDGZ genes.gff.gz
    fi

    if [[ ! -f "protein.fa" ]]; then
        ln -s $PROTFASTAFILE       protein.fa
    fi

    cd ..
    cd ..
fi






if [[ "$DOSNPEFF" ]]; then
    HASGENOME=`grep ${REFNAME}.genome snpEff.config`

    if [[ -z "$HASGENOME" ]]; then
        echo ""                                        >> snpEff.config
        echo "${REFNAME}.genome: Solanum_lycopersicum" >> snpEff.config
        echo ""                                        >> snpEff.config
    fi

    if [[ ! -d "${DATAFOLDER}/${REFNAME}" ]]; then
        java -Xmx20g -jar snpEff.jar build -gff3 -v ${REFNAME}
    fi

    if [[ ! -f "${DATAFOLDER}/${REFNAME}/${REFNAME}.stats" ]]; then
        java -Xmx20g -jar snpEff.jar len ${REFNAME} | tee ${DATAFOLDER}/${REFNAME}/${REFNAME}.stats
    fi





    set +xeu
    read -r -d '' RUN <<'HERE'
    REFNAME=$1
    vcf=`readlink -f $2`

    set -xeu

    P=$PWD

    bn=`basename $vcf`

    echo $bn

    if [[ ! -d "150/$bn" ]]; then
        echo "RUNNING SNPEFF FOR $bn"
        mkdir 150/$bn
        cd 150/$bn
        echo $PWD

        #java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -c $P/snpEff.config ${REFNAME} -v <(gunzip -c $vcf) > $bn.snpeff.vcf 2>log.log
        gunzip -c $vcf | java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -csvStats -c $P/snpEff.config ${REFNAME} -v > $bn.snpeff.vcf 2>log.log




        #JSON=snpEff_summary.csv.json
        #if [[ ! -f "$JSON" ]]; then
        #    $CSVPARSE $JSON ${OUTFOLDER}/RF_*/snpEff_summary.csv
        #    if [[ -f "$JSON.csv" ]]; then
        #        rm $JSON.csv
        #    fi
        #    $COMPILE $JSON $JSON.csv
        #fi
        #
        #sed -f renamer.lst ${OUTBASENAME}_RFall.json.csv > ${OUTBASENAME}_RFall.json.csv.renamed.csv





        #OUTCSV=snpEff_summary.csv
        ##title    : Count by effects
        ##title    : Count by genomic region
        #OUTTSTV=${OUTCSV}.tstv.csv
        #OUTDNDS=${OUTCSV}.dnds.csv
        #
        #
        #f=${OUTCSV}
        #n=`grep -n "Ts/Tv : All variants" $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO ALL VARIANTS IN $PWD/$f"
        #    exit 1
        #fi
        #m=`cat $f | wc -l`
        #d=$((m-n+1))
        #tail -$d $f > $OUTTSTV
        #
        #
        #f=${OUTCSV}
        #n=`grep -n "title    : Count by effects"        $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO COUNT BY EFFECTS IN $PWD/$f"
        #    exit 1
        #fi
        #m=`grep -n "title    : Count by genomic region" $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$m" ]]; then
        #    echo "NO COUNT BY GENOMIC REGION IN $PWD/$f"
        #    exit 1
        #fi
        #m=$((m-1))
        #d=$((m-n+1))
        #head -$m $f | tail -$d > $OUTDNDS
        #echo " DONE CREATING OUT DNDS $OUTDNDS"
        #
        #
        #f=${OUTTSTV}
        #n=`grep -n "Ts/Tv summary" $f | head -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO TSTV SUMMARY IN $PWD/$f"
        #    exit 1
        #fi
        #m=`cat $f | wc -l`
        #d=$((m-n+1))
        #tail -$d $f > $OUTTSTV.summary.csv
        #
        #
        #f=${OUTDNDS}
        #n=`grep -n "title    : Count by effects"        $f | head -2 | tail -1 | cut -d ":" -f1`
        #if [[ -z "$n" ]]; then
        #    echo "NO COUNT BY EFFECTS IN $PWD/$f"
        #    exit 1
        #fi
        #m=`cat $f | wc -l`
        #d=$((m-n+1))
        #head -$m $f | tail -$d > $OUTDNDS.summary.csv
        #echo " DONE CREATING OUT DNDS SUMMARY $OUTDNDS"
        #

    else
        echo "SNPEFF ALREADY RUN FOR $bn"
    fi

    cd $P
HERE

    set -xeu
    echo "$RUN" > run.sh







    if [[ ! -d "$OUTFOLDER" ]]; then
        mkdir $OUTFOLDER
    fi



    find ${DATAIN}/ -name *.vcf.gz | xargs -I{} -n 1 -P 30 bash -c 'echo {}; bash run.sh '${REFNAME}' {}'

    #java -Xmx20G -jar ../../snpEff.jar eff -c ../../snpEff.config -v ${REFNAME} short2.lst.vcf.gz.simplified.vcf.gz > short2.lst.vcf.gz.simplified.vcf.gz.snpeff.vcf 2>log.log
    #bash run.sh ~/nobackup/vcfmerger/84/short2.lst.vcf.gz.simplified.vcf.gz

fi














if [[ "$DOMERGE" ]]; then
    #JSON=${OUTBASENAME}_merged.json
    #if [[ ! -f "$JSON" ]]; then
    #    $CSVPARSE $JSON ${OUTFOLDER}/RF*/snpEff_summary.csv
    #    if [[ -f "$JSON.csv" ]]; then
    #        rm $JSON.csv
    #    fi
    #    $COMPILE $JSON $JSON.csv
    #fi
    #
    #
    #JSON=${OUTBASENAME}_001.json
    #if [[ ! -f "$JSON" ]]; then
    #    $CSVPARSE $JSON ${OUTFOLDER}/RF_001*/snpEff_summary.csv
    #    if [[ -f "$JSON.csv" ]]; then
    #        rm $JSON.csv
    #    fi
    #    $COMPILE $JSON $JSON.csv
    #fi
    #
    #
    #JSON=${OUTBASENAME}_RF1xx.json
    #if [[ ! -f "$JSON" ]]; then
    #    $CSVPARSE $JSON ${OUTFOLDER}/RF_1*/snpEff_summary.csv
    #    if [[ -f "$JSON.csv" ]]; then
    #        rm $JSON.csv
    #    fi
    #    $COMPILE $JSON $JSON.csv
    #fi


    JSON=${OUTBASENAME}_RFall.json
    if [[ ! -f "$JSON" ]]; then
        $CSVPARSE $JSON ${OUTFOLDER}/RF_*/snpEff_summary.csv
        if [[ -f "$JSON.csv" ]]; then
            rm $JSON.csv
        fi
        $COMPILE $JSON $JSON.csv
    fi


    sed -f renamer.lst ${OUTBASENAME}_RFall.json.csv > ${OUTBASENAME}_RFall.json.csv.renamed.csv



    OUTCSV=${OUTBASENAME}_RFall.json.csv.renamed.csv
    #title    : Count by effects
    #title    : Count by genomic region
    OUTTSTV=${OUTCSV}.tstv.csv
    OUTDNDS=${OUTCSV}.dnds.csv


    f=${OUTCSV}
    n=`grep -n "Ts/Tv : All variants" $f | head -1 | cut -d ":" -f1`
    if [[ -z "$n" ]]; then
        echo "NO ALL VARIANTS IN $PWD/$f"
        exit 1
    fi
    m=`cat $f | wc -l`
    d=$((m-n+1))
    tail -$d $f > $OUTTSTV


    f=${OUTCSV}
    n=`grep -n "title    : Count by effects"        $f | head -1 | cut -d ":" -f1`
    if [[ -z "$n" ]]; then
        echo "NO COUNT BY EFFECTS IN $PWD/$f"
        exit 1
    fi
    m=`grep -n "title    : Count by genomic region" $f | head -1 | cut -d ":" -f1`
    if [[ -z "$m" ]]; then
        echo "NO COUNT BY GENOMIC REGION IN $PWD/$f"
        exit 1
    fi
    m=$((m-1))
    d=$((m-n+1))
    head -$m $f | tail -$d > $OUTDNDS
    echo " DONE CREATING OUT DNDS $OUTDNDS"


    f=${OUTTSTV}
    n=`grep -n "Ts/Tv summary" $f | head -1 | cut -d ":" -f1`
    if [[ -z "$n" ]]; then
        echo "NO TSTV SUMMARY IN $PWD/$f"
        exit 1
    fi
    m=`cat $f | wc -l`
    d=$((m-n+1))
    tail -$d $f > $OUTTSTV.summary.csv


    f=${OUTDNDS}
    n=`grep -n "title    : Count by effects"        $f | head -2 | tail -1 | cut -d ":" -f1`
    if [[ -z "$n" ]]; then
        echo "NO COUNT BY EFFECTS IN $PWD/$f"
        exit 1
    fi
    m=`cat $f | wc -l`
    d=$((m-n+1))
    head -$m $f | tail -$d > $OUTDNDS.summary.csv
    echo " DONE CREATING OUT DNDS SUMMARY $OUTDNDS"




fi


#OUTCSV=${OUTBASENAME}_RFall.json.csv
#
##title    : Count by effects
##title    : Count by genomic region
#OUTTSTV=${OUTCSV}.tstv.csv
#OUTDNDS=${OUTCSV}.dnds.csv
#
#f=${OUTCSV}
#n=`grep -n "Ts/Tv : All variants" $f | head -1 | cut -d ":" -f1`
#m=`cat $f | wc -l`
#d=$((m-n+1))
#tail -$d $f > $OUTTSTV
#
#
#f=${OUTCSV}
#n=`grep -n "title    : Count by effects"        $f | head -1 | cut -d ":" -f1`
#m=`grep -n "title    : Count by genomic region" $f | head -1 | cut -d ":" -f1`
#m=$((m-1))
#d=$((m-n+1))
#head -$m $f | tail -$d > $OUTDNDS
#echo " DONE CREATING OUT DNDS $OUTDNDS"
#
#
#f=${OUTTSTV}
#n=`grep -n "Ts/Tv summary" $f | head -1 | cut -d ":" -f1`
#m=`cat $f | wc -l`
#d=$((m-n+1))
#tail -$d $f > $OUTTSTV.summary.csv
#
#f=${OUTDNDS}
#n=`grep -n "title    : Count by effects"        $f | head -2 | tail -1 | cut -d ":" -f1`
#m=`cat $f | wc -l`
#d=$((m-n+1))
#head -$m $f | tail -$d > $OUTDNDS.summary.csv
#echo " DONE CREATING OUT DNDS SUMMARY $OUTDNDS"
#
#
