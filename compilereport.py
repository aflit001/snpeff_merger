#!/usr/bin/python
import argparse
import os, sys
import copy

import simplejson
import jsonpickle

sys.path.insert(0, '.')
try:
    from parsesummary_html import prettytable

except ImportError:
    print "error importing"
    sys.exit(1)


def main():
    try:
        infile = sys.argv[1]

    except:
        print "no infile given"
        sys.exit(1)

    try:
        outfile = sys.argv[2]

    except:
        print "no output file given"
        sys.exit(1)



    if not os.path.exists( infile ):
        print "infile %s does not exists" % infile
        sys.exit(1)

    if     os.path.exists( outfile ):
        print "output %s file already exists. quitting" % outfile
        sys.exit(1)



    if os.path.getsize( infile ) == 0:
        print "infile %s has size 0" % infile
        sys.exit(2)

    if not infile.endswith( '.json' ):
        print "infile %s is not json" % infile
        sys.exit(1)



    grouper =   [
                    #{
                    #    'keys' : ('effects', 'Number of effects by type and region'),
                    #    'poses': 0,
                    #    'dst'  : parseEffectsType
                    #},
                    #{
                    #    'keys' : ('effects', 'Number of effects by type and region'),
                    #    'poses': 1,
                    #    'dst'  : parseEffectsRegion
                    #}


                    { 'keys': ('Count by effects'               , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Count by genomic region'        , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('InDel lengths'                  , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Ts/Tv summary'                  , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Ts/Tv : All variants'           , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Allele frequency : All variants', ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Chromosome change table'        , None), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Effects by functional class'    , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Changes by type'                , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] },
                    { 'keys': ('Change rate by chromosome'      , ''  ), 'dst': [parseStatsCalc, parseStatsRaw] }
                ]



    for group in grouper:
        if not isinstance(group['keys'], list):
            group['keys' ] = [ group['keys'] ]

        if 'poses' not in group:
            group['poses'] = []

        if not isinstance(group['poses'], list):
            group['poses'] = [ group['poses'] ]

        if not isinstance(group['dst'], list):
            group['dst'  ] = [ group['dst'] ]

        if 'data' not in group:
            group['data' ] = {}




    print " acquiring db"
    jsonpickle.set_preferred_backend('simplejson')
    jsonpickle.set_encoder_options(  'simplejson', sort_keys=True, indent=' ', ensure_ascii=True, encoding='utf-8' )

    dic = jsonpickle.decode( open(infile, 'r').read() )
    print " db acquired"



    print " fixing dict"
    #fix tuple dict key being treated as string
    for filename in sorted(dic):
        filekeys = dic[ filename ].keys()
        for key in sorted(filekeys):
            nkey = eval(key)
            dic[filename][nkey] = dic[filename][key]
            dic[filename].pop(key)
    print " dict fixed"



    print " grouping"
    countFiles = 0
    numFiles   = len(dic)
    for filename in sorted(dic):
        countFiles += 1
        print "  grouping %3d/%3d - %s" % ( countFiles, numFiles, filename )

        filekeys = dic[ filename ]
        for key in sorted(filekeys):
            print "  grouping %3d/%3d - %s :: %s" % ( countFiles, numFiles, filename, key )
            tables = filekeys[ key ]

            #for tablepos in range( len( tables ) ):
                #print "%s :: %s :: table type: %s" % ( filename, key, type( tables[tablepos] ) )

            for group in grouper:
                keys = group['keys']

                if keyinkeys(key, keys):
                    print "  grouping %3d/%3d - %s :: %s :: grouping key" % ( countFiles, numFiles, filename, key )

                    gpos  = group['poses']


                    if filename not in group[ 'data' ]:
                        group[ 'data' ][ filename ] = {}
                    if key      not in group[ 'data' ][ filename ]:
                        group[ 'data' ][ filename ][key] = {}


                    if len(gpos) == 0:
                        for gposp in range( len(tables) ):
                            if gposp    not in group[ 'data' ][ filename ]:
                                group[ 'data' ][ filename ][key][gposp] = []
                            group[ 'data' ][ filename ][key][gposp].append( tables[ gposp ] )

                    else:
                        for gposp in gpos:
                            if len(tables) > gposp:
                                if gposp    not in group[ 'data' ][ filename ]:
                                    group[ 'data' ][ filename ][key][gposp] = []

                                group[ 'data' ][ filename ][key][gposp].append( tables[ gposp ] )

                            else:
                                print "key %s gpos %d > %d" % (key, gposp, len(tables))
                                sys.exit(1)

    print "grouped"

    #sys.exit(0)

    print "exporting"
    res = []
    for group in grouper:
        for func in group['dst']:
            resp = func( copy.copy( group ) )
            res.extend( resp )

    with open(outfile, 'w') as ofhd:
        for resp in sorted(res, key=lambda res: str(res[0][0])+str(res[0][1]) ) :
            key, gpos, csv = resp
            print "exporting %s : %s" % ( str(key), str(gpos) )
            ofhd.write("#title    : %s\n" % key[0])
            ofhd.write("#name     : %s\n" % key[1])
            ofhd.write("#table num: %d\n" % gpos  )
            ofhd.write(csv)
            ofhd.write("\n\n")
    print "exported"


def keyinkeys(key, keys):
    if key in keys:
        return True

    for kkey in keys:
        if kkey[1] is None:
            if kkey[0] == key[0]:
                return True

    return False

def parseStatsRaw(groupnfo, raw=True, fname='parseStatsRaw'):
    #return []
    return parseStats(groupnfo, raw=raw, fname=fname)

def parseStatsCalc(groupnfo, raw=False, fname='parseStatsCalc'):
    return parseStats(groupnfo, raw=raw, fname=fname)

def parseStats(groupnfo, raw=False, fname='parseStats'):
    print "parsing :: %s" % fname
    keys = groupnfo['keys' ]
    pose = groupnfo['poses']
    data = groupnfo['data' ]

    print "parsing :: %s :: keys  : %s" % ( fname, keys      )
    print "parsing :: %s :: pose  : %s" % ( fname, pose      )
    print "parsing :: %s :: tables: %d" % ( fname, len(data) )

    stats = {}

    res   = []

    #group[ 'data' ][ filename ][key][gposp].append( tables[ gposp ] )
    for filename in sorted(data):
        #print "parsing :: %s :: filename: %s" % ( fname, filename )
        dkeys = data[filename]

        for key in sorted(dkeys, key=lambda dkeys: dkeys[0]):
            print "parsing :: %s :: filename: %s :: key: %s" % ( fname, filename, key )
            gposes = dkeys[key]

            if key not in stats:
                stats[key] = {}

            for gpos in sorted(gposes):
                if gpos not in stats[key]:
                    stats[key][gpos] = []

                #print "parsing :: %s :: filename: %s :: key: %s :: gpos: %d" % ( fname, filename, key, gpos )
                tables = gposes[gpos]
                #print "parsing :: %s :: filename: %s :: key: %s :: gpos: %d :: tables %s" % ( fname, filename, key, gpos, [x.tocsv(sep=',', raw=raw) for x in tables] )

                if len(stats[key][gpos]) != len(tables):
                    for tpos in range( len(tables) ):
                        stats[key][gpos].append( prettytable('merged', key[0], key[1], ignoreblank=True) )

                for tpos in range( len(tables) ):
                    stats[key][gpos][tpos].add( tables[tpos] )

    for key in sorted(stats):
        for gpos in sorted(stats[key]):
            for table in stats[key][gpos]:
                print "parsing :: %s :: exporting :: %s : %s" % ( fname, key, gpos )
                csv = table.tocsv( sep=',', raw=raw )
                #print csv
                res.append( [key, gpos, csv] )

    return res


if __name__ == '__main__': main()
