#!/bin/bash


rm *.png *.pdf *.median.*.?sv
./median_polish.py




export prefix=median_polish.py.lycopersicum_subtract;

rm $prefix.00.*;
cat $prefix.??.tsv > $prefix.00.tsv;
sed -i '/^\s*$/d' $prefix.00.tsv;
cat $prefix.01.tsv.median.grand_effect.tsv > $prefix.00.tsv.median.grand_effect.tsv;
for m in col_effects original residuals row_effects valids_bool valids_vals; do
    cat $prefix.??.tsv.median.$m.tsv >> $prefix.00.tsv.median.$m.tsv;
    sed -i '/^\s*$/d' $prefix.00.tsv.median.$m.tsv;
done;

./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv                                                        -pdf $prefix.00.tsv
./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_snp.csv  -o ${prefix}.00.tsv.order_snp.tsv  -pdf $prefix.00.tsv
./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_tree.csv -o ${prefix}.00.tsv.order_tree.tsv -pdf $prefix.00.tsv




export prefix=median_polish.py.lycopersicum;

rm $prefix.00.*;
cat $prefix.??.tsv > $prefix.00.tsv;
sed -i '/^\s*$/d' $prefix.00.tsv;
cat $prefix.01.tsv.median.grand_effect.tsv > $prefix.00.tsv.median.grand_effect.tsv;
for m in col_effects original residuals row_effects valids_bool valids_vals; do
    cat $prefix.??.tsv.median.$m.tsv >> $prefix.00.tsv.median.$m.tsv;
    sed -i '/^\s*$/d' $prefix.00.tsv.median.$m.tsv;
done;

./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv                                                        -pdf $prefix.00.tsv
./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_snp.csv  -o ${prefix}.00.tsv.order_snp.tsv  -pdf $prefix.00.tsv
./median_polish.py -p -fsa 11 -T 'S. lycopersicum' -xr 90 -cf names_cols.csv -rf names_rows.csv -ref order_tree.csv -o ${prefix}.00.tsv.order_tree.tsv -pdf $prefix.00.tsv




