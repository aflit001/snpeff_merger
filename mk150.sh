FILTER=/home/assembly/nobackup/vcfmerger/vcffiltergff.py
CSVPARSE=~/nobackup/snpeff/parsesummary_csv.py
COMPILE=~/nobackup/snpeff/compilereport.py

if [[ ! -d 'data' ]]; then
    mkdir data
fi
cd    data

if [[ ! -d "Slyc2.40" ]]; then
    mkdir Slyc2.40
fi
cd    Slyc2.40

if [[ ! -f "ITAG2.3_gene_models.gff3" ]]; then
    wget ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG2.3_release/ITAG2.3_gene_models.gff3
fi
if [[ ! -f "S_lycopersicum_chromosomes.2.40.fa.gz" ]]; then
    wget ftp://ftp.solgenomics.net/tomato_genome/assembly/build_2.40/S_lycopersicum_chromosomes.2.40.fa.gz
fi


if [[ ! -f "fixgff.py" ]]; then
PSTR=<<'HERE'
#!/usr/bin/python

import os, sys

try:
        infile = sys.argv[1]
except:
        sys.exit( 1 )

with open(infile, 'r') as fhd:
        for line in fhd:
                line = line.strip()
                if len(line) == 0  :
                        print line
                        continue

                if line[0]   == "#":
                        print line
                        continue

                cols = line.split("\t")

                start = int(cols[3])
                end   = int(cols[4])

                if start > end:
                        cols[4] = start
                        cols[3] = end

                print "\t".join([str(x) for x in cols])
HERE
echo $PSTR > fixgff.py
fi

if [[ ! -f "ITAG2.3_gene_models.gff3.fixed.gff3" ]]; then
    python fixgff.py ITAG2.3_gene_models.gff3 > ITAG2.3_gene_models.gff3.fixed.gff3
fi
if [[ ! -f "ITAG2.3_gene_models.gff3.fixed.gff3.gz" ]]; then
gzip -c -1 ITAG2.3_gene_models.gff3.fixed.gff3 > ITAG2.3_gene_models.gff3.fixed.gff3.gz
fi

ln -s ITAG2.3_gene_models.gff3.fixed.gff3    genes.gff
ln -s ITAG2.3_gene_models.gff3.fixed.gff3.gz genes.gff.gz
ln -s S_lycopersicum_chromosomes.2.40.fa.gz  sequences.fa.gz

cd ..
cd ..



echo ""                                      >> snpEff.config
echo "Slyc2.40.genome: Solanum_lycopersicum" >> snpEff.config
echo ""                                      >> snpEff.config

if [[ ! -d "data/Slyc2.40" ]]; then
    java -Xmx20g -jar snpEff.jar build -gff3 -v Slyc2.40
fi
if [[ ! -f "data/Slyc2.40/Slyc2.40.stats" ]]; then
    java -Xmx20g -jar snpEff.jar len Slyc2.40 | tee data/Slyc2.40/Slyc2.40.stats
fi


read -r -d '' RUN <<'HERE'
gff=$1

P=$PWD

bn=`basename $gff`

echo $bn

if [[ ! -d "150/$bn" ]]; then
    echo "RUNNING SNPEFF FOR $bn"
	mkdir 150/$bn
	cd 150/$bn
	echo $PWD
	java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -c $P/snpEff.config Slyc2.40 -v $gff >$bn.snpeff.vcf 2>log.log
else
    echo "SNPEFF ALREADY RUN FOR $bn"
fi

cd $P
HERE
echo "$RUN" > run.sh

if [[ ! -d "150" ]]; then
    mkdir 150
fi



find ~/tomato150/reseq/mapped/Heinz/ -name *.vcf.gz | xargs -I{} -n 1 -P 10 bash -c 'echo {}; bash run.sh {}'

#java -Xmx20G -jar ../../snpEff.jar eff -c ../../snpEff.config -v Slyc2.40 short2.lst.vcf.gz.simplified.vcf.gz > short2.lst.vcf.gz.simplified.vcf.gz.snpeff.vcf 2>log.log
#bash run.sh ~/nobackup/vcfmerger/84/short2.lst.vcf.gz.simplified.vcf.gz



JSON=snpEff_150_summary_merged.json
if [[ ! -f "$JSON" ]]; then
    $CSVPARSE $JSON 150/m*/snpEff_summary.csv
    if [[ -f "$JSON.csv" ]]; then
        rm $JSON.csv
    fi
    $COMPILE $JSON $JSON.csv
fi


JSON=snpEff_150_summary_001.json
if [[ ! -f "$JSON" ]]; then
    $CSVPARSE $JSON 150/RF_001*/snpEff_summary.csv
    if [[ -f "$JSON.csv" ]]; then
        rm $JSON.csv
    fi
    $COMPILE $JSON $JSON.csv
fi


JSON=snpEff_150_summary_RF1xx.json
if [[ ! -f "$JSON" ]]; then
    $CSVPARSE $JSON 150/RF_1*/snpEff_summary.csv
    if [[ -f "$JSON.csv" ]]; then
        rm $JSON.csv
    fi
    $COMPILE $JSON $JSON.csv
fi


JSON=snpEff_150_summary_RFall.json
if [[ ! -f "$JSON" ]]; then
    $CSVPARSE $JSON 150/RF_*/snpEff_summary.csv
    if [[ -f "$JSON.csv" ]]; then
        rm $JSON.csv
    fi
    $COMPILE $JSON $JSON.csv
fi


sed -f renamer.lst snpEff_summary_csv_RFall.json.csv > snpEff_summary_csv_RFall.json.csv.renamed.csv
