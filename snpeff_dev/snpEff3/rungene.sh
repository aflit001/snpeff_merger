set -xeu

gff=$1
vcf=$2

P=$PWD

gff=`readlink -f $gff`
vcf=`readlink -f $vcf`
bn=`basename $vcf`

echo $bn

OVCF=${gff}_${bn}.filtered.vcf.gz

if [[ ! -f "genes_out/$bn/$OVCF" ]]; then
    if [[ ! -d "genes_out/$bn" ]]; then
        mkdir genes_out/$bn
    fi


	cd genes_out/$bn


    if [[ ! -f "$OVCF" ]]; then
        echo "FILTERING $vcf ACCORDING TO $gff TO $OVCF"
        python /home/assembly/nobackup/vcfmerger/vcffiltergff.py -g $gff -i $vcf -o $OVCF 2>&1 | tee $OVCF.log
    else
        echo "FILTERED $vcf ACCORDING TO $gff TO $OVCF"
    fi


    echo $PWD
    if [[ ! -f "$OVCF" ]]; then
        echo "ERROR CREATING $OVCF"
        exit 1
    fi


    OVCFEFF=$OVCF.snpeff.vcf
    OCSVEFF=$OVCF.snpeff.csv

    if [[ ! -f "$OVCFEFF" ]]; then
        echo "RUNNING SNP EFFECT IN $OVCF TO $OVCFEFF"
        java -Xmx20g -jar $P/snpEff.jar eff -no-upstream -no-downstream -ud 0 -csvStats -c $P/snpEff.config Slyc2.40 -v $OVCF -s $OCSVEFF >$OVCFEFF 2>log.log

    else
        echo "SNP EFFECT ALREADY RUN IN $OVCF TO $OVCFEFF"

    fi


    if [[ ! -f "$OVCFEFF" ]]; then
        echo "ERROR CREATING $OVCFEFF"
        exit 1
    fi


    if [[ ! -f "$OCSVEFF" ]]; then
        echo "ERROR CREATING $OCSVEFF"
        exit 1
    fi
fi

cd $P
