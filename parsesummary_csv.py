#!/usr/bin/python

print "importing"
import argparse
import os, sys
import re

import array
import numpy
import types
import copy

import simplejson
import jsonpickle
import unicodedata

print "setting up json pickle"
jsonpickle.set_preferred_backend('simplejson')
jsonpickle.set_encoder_options(  'simplejson', sort_keys=True, indent=' ', ensure_ascii=True, encoding='utf-8' )


print "import parse summary html"
sys.path.insert(0, '.')
import parsesummary_html
from parsesummary_html import parsetext

noheaders      = []
hassummary     = []
specialparsers = {}

def main():
    try:
        outfile = sys.argv[1]

    except:
        print "no outfile given"
        sys.exit(1)


    if os.path.exists( outfile ):
        print "oufile exists"
        sys.exit(1)


    try:
        infiles = sys.argv[2:]

    except:
        print "no infiles"
        sys.exit(1)




    global noheaders
    global specialparsers

    #'Chromosome change table','Coverage', 'InDel lengths', 'Quality',
    noheaders      = ['Ts/Tv summary', 'Ts/Tv : Known variants', 'Ts/Tv summary' ]
    specialparsers = {
                        'Chromosome change table'    : parser_histogram                  ,
                        'Amino acid change table'    : parser_Amino_acid_change_table    ,
                        'Change rate by chromosome'  : parser_Amino_acid_change_table    ,
                        'Base changes'               : parser_Amino_acid_change_table    ,
                        'Codon change table'         : parser_Amino_acid_change_table    ,
                        'Coverage'                   : parser_histogram_line             ,
                        'InDel lengths'              : parser_histogram_line             ,
                        'Quality'                    : parser_histogram_line             ,
                        'Summary table'              : parser_summary                    ,
                        'Ts/Tv : All variants'       : parser_tstv                       ,
                        'Ts/Tv : Known variants'     : parser_tstv                       ,
                        'Ts/Tv summary'              : parser_tstv                       ,
                        'Effects by functional class': parser_Effects_by_functional_class
                    }



    dic = {}

    for filename in infiles:
        print "checking %3d/%3d - %s" % ( infiles.index(filename)+1, len(infiles), filename )
        if not os.path.exists( filename ):
            print "infile %s does not exists" % filename
            sys.exit(1)

        if os.path.getsize( filename ) == 0:
            print "infile %s has size 0" % filename
            sys.exit(2)

        if not filename.endswith( '.csv' ):
            print "infile %s is not csv" % filename
            sys.exit(1)




    for filename in sorted(infiles):
        print "parsing  %3d/%3d - %s" % ( infiles.index(filename)+1, len(infiles), filename )

        dic[ filename ] = {}

        lines = open(filename, 'r').read().split("\n")

        title = None

        raw   = {}
        for line in lines:
            line = line.strip()
            #print "LINE", line

            if len(line) == 0: continue

            if line[0] == "#":
                title      = line[1:].strip()
                #print "TITLE", title
                raw[title] = []
                continue

            if title is None: continue

            raw[title].append(line)


        #sys.exit(0)
        for title in sorted(raw):
            lines = raw[title]
            lines = splitall(lines)
            print "parsing  %3d/%3d - %s :: title: %s" % ( infiles.index(filename)+1, len(infiles), filename, title )

            if len(lines) == 0:
                print "parsing  %3d/%3d - %s :: title: %s :: length 0. skipping" % ( infiles.index(filename)+1, len(infiles), filename, title )
                continue

            if title in specialparsers:
                specialparsers[ title ]( dic[ filename ], filename, title, lines, prename="%3d/%3d" % (infiles.index(filename)+1, len(infiles)) )

            else:
                parser_default( dic[ filename ], filename, title, lines, prename="%3d/%3d" % (infiles.index(filename)+1, len(infiles)) )

        #sys.exit(0)



    print "DONE READING. exporting"

    with open(outfile, 'w') as ofhd:
        print "converting"
        jsonstr = jsonpickle.encode( dic )
        print "converted"
        print len(jsonstr)
        print "saving"
        ofhd.writelines( jsonstr.splitlines(True) )
        print "saved"

    print "finished"



def splitall(lines):
    for linepos in range(len(lines)):
        lines[linepos] = [parsetext(x) for x in lines[linepos].split(",")]
    return lines

def parser_default(dic, filename, title, lines, name="", labelsPos=[0], prename=""):
    headers = []

    if title not in noheaders:
        cols = lines.pop(0)
        headers.append( cols )

    tdata   = parsesummary_html.prettytable(filename, title, "")

    print "parsing  %s - %s :: title: %s :: name: %s" % ( prename, filename, title, name )
    # add headers
    for header in headers:
        print "parsing  %s - %s :: title: %s :: header: %s (%d)" % ( prename, filename, title, str(header), len(header) )
        tdata.addHeader( header )

    for cols in lines:
        print "parsing  %s - %s :: title: %s :: cols: %s (%d)" % ( prename, filename, title, str(cols), len(cols) )
        tdata.addLine( cols, lebelsPos=labelsPos )

    if title not in dic:
        dic[ (title, name) ] = []

    dic[ (title, name) ].append( tdata )

def parser_histogram(dic, filename, title, lines, name="", prename=""):
    print "parsing  %s - %s :: title: %s :: name: %s - HISTOGRAM" % ( prename, filename, title, name )

    keys   = {}

    for linepos in range(len(lines)):
        linename = lines[linepos][0]

        if linename not in keys:
            keys[linename] = []

        ldata = lines[linepos][1:]
        keys[linename].append( ldata )

    for name in keys:
        llines = keys[name]
        parser_histogram_line(dic, filename, title, llines, name=name, prename=prename)

def parser_histogram_line(dic, filename, title, lines, name="", prename=""):
    print "parsing  %s - %s :: title: %s :: name: %s - HISTOGRAM - LINE" % ( filename, title, name, prename )

    llines = lines
    llines = map(list, zip(*llines))
    #print llines

    parser_default(dic, filename, title, llines, name=name, prename=prename)

def parser_Amino_acid_change_table(dic, filename, title, lines, name="", prename=""):
    print "parsing  %s - %s :: title: %s :: name: %s - AA CHANGE" % ( prename, filename, title, name )
    print lines

    for linepos in range(0, len(lines)):
        lines[linepos] = lines[linepos][1:]

    parser_default(dic, filename, title, lines, name=name, prename=prename)

def parser_summary(dic, filename, title, lines, name="", prename=""):
    print "parsing  %s - %s :: title: %s :: name: %s - SUMMARY" % ( prename, filename, title, name )

    for pos in [3, 12]:
        lines.append( lines[pos][0:1] + lines[pos][2:3] )
        lines[-1][0] += " " + str(pos)
        lines[pos] = lines[pos][0:2]

    parser_default(dic, filename, title, lines, name=name, prename=prename)

def parser_tstv(dic, filename, title, lines, name="", prename=""):
    print "parsing  %s - %s :: title: %s :: name: %s - TSTV" % ( prename, filename, title, name )

    if len(lines) != 1:
        parser_default(dic, filename, title, lines, name=name, prename=prename)

def parser_Effects_by_functional_class(dic, filename, title, lines, name="", prename=""):
    print "parsing  %s - %s :: title: %s :: name: %s - EFF FUNC CLASS" % ( prename, filename, title, name )

    lines[-1].append(lines[-1][0])
    parser_default(dic, filename, title, lines, name=name, prename=prename)


if __name__ == '__main__': main()
