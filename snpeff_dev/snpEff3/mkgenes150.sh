set -xeu

VCFFOLDER=genes_bam
GFFFOLDER=genes_in
OUTFOLDER=genes_out

FILTER=/home/assembly/nobackup/vcfmerger/vcffiltergff.py
CSVPARSE=~/nobackup/snpeff/parsesummary_csv.py
COMPILE=~/nobackup/snpeff/compilereport.py


set +xeu
read -r -d '' RUN <<HERE
set -xeu

gff=\$1
vcf=\$2

P=\$PWD

gff=\`readlink -f \$gff\`
vcf=\`readlink -f \$vcf\`
bn=\`basename \$vcf\`

echo \$bn

OVCF=\${gff}_\${bn}.filtered.vcf.gz

if [[ ! -f "$OUTFOLDER/\$bn/\$OVCF" ]]; then
    if [[ ! -d "$OUTFOLDER/\$bn" ]]; then
        mkdir $OUTFOLDER/\$bn
    fi


	cd $OUTFOLDER/\$bn


    if [[ ! -f "\$OVCF" ]]; then
        echo "FILTERING \$vcf ACCORDING TO \$gff TO \$OVCF"
        python $FILTER -g \$gff -i \$vcf -o \$OVCF 2>&1 | tee \$OVCF.log
    else
        echo "FILTERED \$vcf ACCORDING TO \$gff TO \$OVCF"
    fi


    echo \$PWD
    if [[ ! -f "\$OVCF" ]]; then
        echo "ERROR CREATING \$OVCF"
        exit 1
    fi


    OVCFEFF=\$OVCF.snpeff.vcf
    OCSVEFF=\$OVCF.snpeff.csv

    if [[ ! -f "\$OVCFEFF" ]]; then
        echo "RUNNING SNP EFFECT IN \$OVCF TO \$OVCFEFF"
        java -Xmx20g -jar \$P/snpEff.jar eff -no-upstream -no-downstream -ud 0 -csvStats -c \$P/snpEff.config Slyc2.40 -v \$OVCF -s \$OCSVEFF >\$OVCFEFF 2>log.log

    else
        echo "SNP EFFECT ALREADY RUN IN \$OVCF TO \$OVCFEFF"

    fi


    if [[ ! -f "\$OVCFEFF" ]]; then
        echo "ERROR CREATING \$OVCFEFF"
        exit 1
    fi


    if [[ ! -f "\$OCSVEFF" ]]; then
        echo "ERROR CREATING \$OCSVEFF"
        exit 1
    fi
fi

cd \$P
HERE
set -xeu

echo "$RUN" > rungene.sh


if [[ ! -d "$VCFFOLDER" ]]; then
    echo "VCFFOLDER $VCFFOLDER DOES NOT EXISTS"
    exit 1
fi

if [[ ! -d "$GFFFOLDER" ]]; then
    echo "GFFFOLDER $GFFFOLDER DOES NOT EXISTS"
    exit 1
fi

if [[ ! -d "$OUTFOLDER" ]]; then
    mkdir $OUTFOLDER
fi

set +xeu







read -r -d '' RUN <<HERE
set -xeu
ingff=\$1

find $VCFFOLDER -maxdepth 1 -name '*.vcf.gz' | sort | xargs -I{} -n 1 -P 10 bash -c 'echo PARSING GFF GENE {}; bash rungene.sh '\${ingff}' {}'

OUTJSON=\${ingff}.json
OUTCSV=\${ingff}.json.csv

if [[ ! -f "\${OUTJSON}"   ]]; then
    $CSVPARSE \$OUTJSON \${ingff}*.snpeff.csv
fi

if [[ ! -f "\${OUTJSON}"   ]]; then
    echo " error creating json"
    exit 1
fi

if [[ ! -f "\${OUTCSV}"   ]]; then
    #echo \$COMPILE \$OUTJSON \$OUTCSV
    $COMPILE \$OUTJSON \$OUTCSV
fi

if [[ ! -f "\${OUTCSV}"   ]]; then
    echo " error creating csv"
    exit 1
fi


#title    : Count by effects
#title    : Count by genomic region
OUTTSTV=\${OUTCSV}.tstv.csv
OUTDNDS=\${OUTCSV}.dnds.csv

f=\${OUTCSV}
n=\`grep -n "Ts/Tv : All variants" \$f | head -1 | cut -d ":" -f1\`
m=\`cat \$f | wc -l\`
d=\$((m-n+1))
tail -\$d \$f > \$OUTTSTV


f=\${OUTCSV}
n=\`grep -n "title    : Count by effects"        \$f | head -1 | cut -d ":" -f1\`
m=\`grep -n "title    : Count by genomic region" \$f | head -1 | cut -d ":" -f1\`
m=\$((m-1))
d=\$((m-n+1))
head -\$m \$f | tail -\$d > \$OUTDNDS
echo " DONE CREATING OUT DNDS $OUTDNDS"


f=\${OUTTSTV}
n=\`grep -n "Ts/Tv summary" \$f | head -1 | cut -d ":" -f1\`
m=\`cat \$f | wc -l\`
d=\$((m-n+1))
tail -\$d \$f > \$OUTTSTV.summary.csv

f=\${OUTDNDS}
n=\`grep -n "title    : Count by effects"        \$f | head -2 | tail -1 | cut -d ":" -f1\`
m=\`cat \$f | wc -l\`
d=\$((m-n+1))
head -\$m \$f | tail -\$d > \$OUTDNDS.summary.csv
echo " DONE CREATING OUT DNDS SUMMARY $OUTDNDS"

HERE
set -xeu

echo "$RUN" > rungenegff.sh






find $GFFFOLDER -maxdepth 1 -name '*.gff3' | sort | xargs -I{} -n 1 -P 1 bash -c 'echo PARSING GFF DATA {}; bash rungenegff.sh {}'


cat $GFFFOLDER/*.tstv.csv             > $GFFFOLDER/tstv.csv
cat $GFFFOLDER/*.tstv.csv.summary.csv > $GFFFOLDER/tstv.csv.summary.csv


cat $GFFFOLDER/*.dnds.csv             > $GFFFOLDER/dnds.csv
cat $GFFFOLDER/*.dnds.csv.summary.csv > $GFFFOLDER/dnds.csv.summary.csv
grep -E '#name|Type,Count|Type,Count - max|SYNONYMOUS'  $GFFFOLDER/dnds.csv > $GFFFOLDER/dnds.csv.filtered.csv

cp genes_in/dnds* genes_in/tstv* .

#JSON=snpEff_summary_$bn.json
#rm $JSON
#./parsesummary_csv.py $JSON $OUTFOLDER/*/snpEff_summary.csv
#rm $JSON.csv
#./compilereport.py $JSON $JSON.csv
