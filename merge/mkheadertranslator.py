#!/usr/bin/python

import os
import sys
import gzip

def openfile( fn, fm ):
	if fn.endswith( '.gz' ):
		return gzip.open( fn, fm + 'b' )
	else:
		return open( fn, fm )
		
def fixname( fn ):
	return fn.replace(" ", "\\ ").replace("/", "\\/").replace("(", "\(").replace(")", "\)").replace(".", "\.")


def main():
	##INFO=<ID=EFF,Number=.,Type=String,Description="Predicted effects for this variant.Format: 'Effect ( Effect_Impact | Functional_Class | Codon_Change | Amino_Acid_Change| Amino_Acid_length | Gene_Name | Transcript_BioType | Gene_Coding | Transcript_ID | Exon_Rank  | Genotype_Number [ | ERRORS | WARNINGS ] )' ">
	#CHROM  POS     ID      REF     ALT     QUAL    FILTER  INFO    FORMAT  /panfs/ANIMAL/group001/minjiumeng/tomato_reseq/SZAXPI008746-45
	#FORMAT  /panfs/ANIMAL/group001/minjiumeng/tomato_reseq/SZAXPI008746-45

	inpfile = sys.argv[ 1 ]


	translator = {}
	with open( inpfile         , 'r' ) as fhd:
		for line in fhd:
			cols = line.strip().split( "\t" )
			translator[ cols[0] ] = cols[ 1 ]

	print translator

	fo = open( inpfile+'.out', 'w' )

	for file in sys.argv[2:]:
		print "parsing file"

		with openfile( file, 'r' ) as fhd:
			for line in fhd:
				if len( line ) > 0 and line[0] == "#" and line[:2] != "##" and "FORMAT" in line:
					line = line.strip()
					print line

					cols = line.split("\t")

					print cols

					fn = cols[ -1 ]

					print fn

					filebn = os.path.basename( file ).replace( '.snpeff.vcf.gz', '' )

					print filebn

					if filebn in translator:
						print "translating", file, filebn, translator[ filebn ]
						file = translator[ filebn ]

					fns   = fixname( fn   )
					files = fixname( file )

					print fns
					print files

					fo.write( "s/%s/%s/g\n" % ( fns, files ) )

					break
					
if __name__ == "__main__": main()
